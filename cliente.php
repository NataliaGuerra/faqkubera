<!doctype html>
<html lang="es">
    <head>
      <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title>Centro de Ayuda</title>
        <link rel="shortcut icon" href="assets/img/favicon.ico" />
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:800,700,600,400' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="assets/css/estilos.css">
        <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
        <link rel="stylesheet" type="text/css" href="assets/css/acordion.css">
        
    </head>
    <body>
        <!-- Comienza el Body -->
        <!-- Comienza Header -->
        <header class="navbar-header header">
            <div class="wrapper">
            <div class="logo" style="margin-top: 26px;"></div>
            <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-navbar" aria-controls="bs-navbar" aria-expanded="false"> 
            <span class="sr-only">Toggle navigation</span> 
            <span class="icon-bar"></span> 
            <span class="icon-bar"></span> 
            <span class="icon-bar"></span> 
            </button>
            <nav class="navbar-collapse collapse">
            <a id="buscar" class="amarillo" href="#">Buscar<i class="fa buscar-icon"></i></a>
            <a id="unirse" class="amarillo" href="#">Unirse<i class="fa unirse-icon"></i></a>
            <a id="ingresar" class="amarillo" href="#">Ingresar<i class="fa ingresar-icon"></i></a>
            </nav>
            </div>
            <nav style="height: 1px;" aria-expanded="false" id="bs-navbar" class="collapse"> 
            <ul class="nav navbar-nav" style="text-align: center;"> 
            <li class="active"><a href="#">Buscar<i class="fa buscar-icon"></i></a></li> 
            <li><a href="#">Unirse<i class="fa unirse-icon"></i></a></li>
            <li><a href="#">Ingresar<i class="fa ingresar-icon"></i></a></li> 
            </ul> 
            </nav>
        </header>
        <!-- Termina Header -->
        <!-- Comienza el contendor de la pagina -->
        <div class="container">     
            <div class="col-md-12">
            <br>
                <FONT COLOR="#6b3f23"><b><i class="fa fa-chevron-left"></i> Inicio / El Servicio / Cómo Funciona</b></FONT>
                <b><h1 style="font-size: 45px;font-weight: 700;margin-top: 20px;">Bienvenido al centro de ayuda</h1></b>
                <FONT style="font-size: 40px;font-weight: normal;" FACE="arial" SIZE=5 COLOR="#6b3f23"><h3 style="
                  margin-left: 340px; font-size: 31px;">¡Nos gusta que te vaya bien!</h3></FONT>
                <center><iframe style="margin-top: 10px;" width="750" height="400" src="http://www.youtube.com/embed/"></iframe></center>
                <div class="container buscar" style="width: 80%;">
                <form id="form" name="form" class="buscar-consultor" action="buscar.php" method="POST" style="position: relative;">
                    <div id="selection" class="blanco"  style="font-size: 16px; font-weight:600">¿En qué podemos ayudarte?</div><br>
                        <input id="mainSearcherBox" value="" name="mainSearcherBox" class="busqueda" type="text" placeholder="Escribe aquí el tema sobre el que necesitas más información" onkeydown="">
                        <button style="font-size: 14px;" class="empezar-busqueda" type="submit">Buscar</button>
                    <div id="unibox-suggest-box" style="min-width: 810px; max-width: 398px;"></div><div id="unibox-invisible"></div></form>
                </div>
                <div class="bottom-arrow"></div>
                <div class="col-md-12 text-center">
                   <center><div style="min-width: 310px; max-width: 198px;  margin-left:250px; margin-top:30px" id="clientes" class="col-md-7">
                        <img id="clientesImg" style="margin-left: -40px;" src="assets/img/ico_clientes.png">
                        <h3 style="margin-left: -40px;" id="h3Clientes" class="amarillo">Clientes</h3>
                    </div>
                    </center>
                    <center> 
                    <div style="margin-top:30px" id="consultores" class="col-md-1">
                        <img id="consultor" src="assets/img/ico_consultores.png">
                        <h3 style="margin-left: 58px;" id="h3Consultores" class="cafe">Consultores</h3>
                    </div>
                    </center>
                </div>
                   <hr id="hr1" style="width: 60%; margin-left: 220px;">
                <button  onclick="mostrar1(this); return false" value="Mostrar1" id="comenzandoCliente2" class="btn btn-lg" onClick="comenzandoCliente2();"><i class="fa fa-chevron-left"></i> COMENZANDO</button>
                 <a id="unirse" class="amarillo" href="#"><i class="fa unirse-icon-6"></i></a>
                
                <script type="text/javascript">
                function mostrar1(enla) {
                  obj = document.getElementById('oculto1');
                  obj.style.display = (obj.style.display == 'block') ? 'none' : 'block';
                  enla.innerHTML = (enla.innerHTML == 'COMENZANDO') ? 'COMENZANDO' : 'COMENZANDO';
                } 
                </script>


                <div style="margin-top:30px" class="text-center">

                <div id='oculto1' style="display:none";> 

                <div class="accordion">
                            <div class="accordion-section">
                                <a class="accordion-section-title active"  id="accordionA" href="#accordion-A">¿Qué es Kubera?</a>
                                <div id style="margin-left:56px" id="accordion-A" class="accordion-section-content">
                                <p class="respuesta"><font id="p1" color="#8A4B08"><hr style="width: 61%;margin-left: 174px;" size="2" color="#F2F2F2" ><center style="width: 62%;margin-left: 171px;text-align: justify;font-size: 18px;">Kubera es la comunidad del intercambio de conocimiento y el desarrollo personal. El punto de encuentro de los que buscan soluciones y los que escuchan, conocen y dan soluciones. De los que crecen y quieren ver a los demás crecer. Para ello, proveemos una plataforma para que los miembros de la comunidad, pongan su conocimiento al servicio de los demás, ofreciendo soluciones en vivo y en directo a través de una video conferencia en línea.</hr></font></p>
                                </center><hr  style="width: 61%;margin-right: 303px;" size="2" color="#F2F2F2" ></hr>
                                <br>
                                    <a id="subindice" class="accordion-section-title" href="#accordion-A1">¿Cómo lo hacemos?</a>
                                    <div id="accordion-A1" class="accordion-section-content">
                                        <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Ayudamos a los consultores a crear su perfil, a identificar sus talentos a crear sus cursos y promocionarlos. Usamos su experiencia para mejorar la plataforma. Conectamos a personas que buscan con los que ofrecen sus servicios dándole a ambos una excelente experiencia de uso.</center></hr></font></p>
                                        <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2"></hr>
                                    </div><!--fin de  .accordion-section-content-->
                                    <a id="subindice" class="accordion-section-title" href="#accordion-A2">¿Qué hacemos?</a>
                                    <div id="accordion-A2" class="accordion-section-content">
                                        <p  class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Proveemos una plataforma para que los miembros de la comunidad, pongan su conocimiento al servicio de los demás, ofreciendo soluciones en vivo y en directo. Les damos un canal de venta. Proveemos una excelente experiencia de usuario para buscadores y oferentes de servicios</center></hr></font></p>
                                        <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                    </div><!--fin de  .accordion-section-content-->
                                </div><!--fin de  .accordion-section-content-->
                            </div><!--fin de  .accordion-section-->
                        </div><!--fin de  .accordion-->
                        <div class="accordion">
                            <div class="accordion-section">
                                <a   class="accordion-section-title active" id="accordionB" href="#accordion-B">¿Por qué lo hacemos?</a>
                                <div style="margin-left:56px;" id="accordion-B" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ><center style="width: 62%;margin-left: 199px;text-align: justify;font-size: 18px;">Queremos que la gente crezca, descubra su talento y le vaya bien. Lideramos una nueva era del trabajo y los negocios impulsada por elintercambio de conocimiento, la generosidad y la curiosidad.</center></hr></font></p>
                                    <hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                            </div><!--fin de  .accordion-section-->
                        </div><!--fin de  .accordion-->
                        <div class="accordion">
                            <div class="accordion-section">
                                <a class="accordion-section-title active" id="accordionC" href="#accordion-C">¿Para quién lo hacemos?</a>
                                <div style="margin-left:56px; " id="accordion-C" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ><center style="width: 62%;margin-left: 199px;text-align: justify;font-size: 18px;">Para toda persona que comparten nuestros valores. Que creen como nosotros que al compartir conocmiento, todos crecemos. Kubera es para los profesionales independientes y también para los emprendedores que quieren compartir su conocimiento con los demás cualquiera que este sea.  Para las personas que buscan soluciones inmediatas, prácticas, tanto para ellas, su familia u hogar como su negocio o empresa.</center></hr></font></p>
                                    <hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                            </div><!--fin de  .accordion-section-->
                        </div><!--fin de  .accordion-->
                        <div class="accordion">
                            <div class="accordion-section">
                                <a class="accordion-section-title active" id="accordionD" href="#accordion-D">¿Cómo ser parte de la comunidad?</a>
                                <div style="margin-left:56px; " id="accordion-D" class="accordion-section-content">
                                <p class="respuesta"><font color="#8A4B08"><hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ><center style="width: 62%;margin-left: 199px;text-align: justify;font-size: 18px;">Ingresa a link unirse como cliente. El proceso de registro es sencillo y gratuito permitiéndote ser parte de la comunidad inmediatamente.</center></hr></font></p>
                                <hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ></hr>
                                    <a  id="subindice2" class="accordion-section-title" href="#accordion-D1">¿Cuánto cuesta ser parte de la comunidad?</a>
                                    <div id="accordion-D1" class="accordion-section-content">
                                        <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Unirte a Kubera es GRATIS.  Puedes ser cliente, consultor o ambas sin costo alguno.  Solo pagas al contratar los servicios de un consultor.</center></hr></font></p>
                                        <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                    </div><!--fin de  .accordion-section-content-->
                                    <a id="subindice2" class="accordion-section-title" href="#accordion-D2">¿Cuáles son los requisitos para estar en Kubera?</a>
                                    <div id="accordion-D2" class="accordion-section-content">
                                        <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Solo tienes que ser mayor de edad para contratar o prestar tus servicios y tienes que tener acceso a internet.</center></hr></font></p>
                                        <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                    </div><!--fin de  .accordion-section-content-->
                                    <a id="subindice2" class="accordion-section-title" href="#accordion-D3">Hay una sección para Clientes y otra para Consultores. ¿Cuál es la diferencia?</a>
                                    <div id="accordion-D3" class="accordion-section-content">
                                        <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Todos somos parte de la comunidad, y todos podemos ser Consultores y Clientes.Valoramos toda forma de conocimiento, desde profesionales especializados hasta sabios del día a día. Puedes prestar tus servicios orientados a personas, familia y hogares o a negocios y empresas. Revisa aquí nuestras categorías   (Link categorías). Si tienes una habilidad o súperpoder, como lo llamamos en Kubera, puedes compartirlo con el resto registrándote como consultor de forma gratuita. Podrás crear tus anuncios y promover tu talento al mundo. Valoramos toda forma de conocimiento. (capturas de pantalla y links).  Si lo que buscas es una solución a temas cotidianos o especializados, regístrate gratis como cliente y encuentra lo que necesitas de entre los anuncios que los especialistas han creado para tí.    (capturas de pantalla y links)</center></hr></font></p>
                                        
                                    </div><!--fin de  .accordion-section-content-->
                                </div><!--fin de  .accordion-section-content-->
                            </div><!--fin de  .accordion-section-->
                        </div><!--fin de  .accordion-->  
                   
                    <br>
                </div>

                <hr id="hr3" style="width: 68%;margin-left: 177px;">

                <button  onclick="mostrar(this); return false" value="Mostrar" id="comenzandoCliente" class="btn btn-lg" onClick="comenzandoCliente();"><i class="fa fa-chevron-left"></i>BUSCANDO SOLUCIONES</button>
                <a id="unirse" class="amarillo" href="#"><i class="fa unirse-icon-5"></i></a>

                <script type="text/javascript">
                function mostrar(enla) {
                  obj = document.getElementById('oculto');
                  obj.style.display = (obj.style.display == 'block') ? 'none' : 'block';
                  enla.innerHTML = (enla.innerHTML == '< BUSCANDO SOLUCIONES') ? '< BUSCANDO SOLUCIONES' : '< BUSCANDO SOLUCIONES';
                }
                </script>

                 <div id='oculto' style="display:block";> 
                    <center><iframe id="video"  style="margin-top: 10px; width: 61%; height: 350px;" width="550" height="300" src="http://www.youtube.com/embed/"></iframe></center>
                    <a id="numero1C" class="amarillo" href="#"><i class="fa numero1C"></i></a>
                    <a id="numero2C" class="amarillo" href="#"><i class="fa numero2C"></i></a>
                    <a id="numero3C" class="amarillo" href="#"><i class="fa numero3C"></i></a>
                    <p class="respuesta-cliente4" style="width: 16%; margin-left: 232px; text-align: center; margin-top:16px; font-size: 15px;"><font color="#8A4B08">Los consultores han creado anuncios y perfiles para que te resulte fácil encontrarlos</font></p>
                    <p class="respuesta-cliente5" style="width: 16%; margin-left: 472px; text-align: center; margin-top:-95px; font-size: 15px;"><font color="#8A4B08">Mira las reseñas, analiza el precio y condiciones del servicio. Contáctalo inmediatamente  cuando esté disponible</font></p>
                    <p class="respuesta-cliente6" style="width: 19%; margin-left: 681px; text-align: center; margin-top:-117px; font-size: 15px;"><font color="#8A4B08">Haz una consulta precisa a tu experto y explícale bien tus expectativas, no olvides hacer una reseña al finalizar la sesión para ayudarlo a él y otros buscadores como tu</font></p>
                    <br>
                    <div style="margin-top:30px" class="text-center"> 
                     <div class="accordion">
                        <div class="accordion-section">
                            <a class="accordion-section-title"  id="accordion100" href="#accordion-100">Cómo Unirme GRATIS a la Comunidad</a>
                            <div style="margin-left:56px" id="accordion-100" class="accordion-section-content">
                                <p class="respuesta"><font id="p1" color="#8A4B08"><hr style="width: 61%;margin-left: 174px;" size="2" color="#F2F2F2" ><center style="width: 62%;margin-left: 171px;text-align: justify;font-size: 18px;">Ingresa a link "Unirse como cliente" o "Unirse como consultor". El proceso de registro es sencillo y gratuito permitiéndote ser parte de la comunidad inmediatamente.</hr></font></p>
                                </center>           
                                <br>
                                <a id="subindice" class="accordion-section-title" href="#accordion-2">¿Puedo ser consultor y Cliente?</a>
                                <div id="accordion-2" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Por supuesto , el objetivo de Kubera es que todos sus miembros compartan su conocimiento, por lo que te motivamos para que impartas y recibas conocimiento tanto como consultor como cliente</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2"></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice" class="accordion-section-title" href="#accordion-3">¿Cómo hago para registrarme como consultor si soy cliente?</a>
                                <div id="accordion-3" class="accordion-section-content">
                                    <p  class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Al ingresar a tu cuenta verás en la esquina superior derecha una opción al lado de tu cuenta para "Aplicar como Consultor"</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice" class="accordion-section-title" href="#accordion-4">¿Cómo hago para contratar servicios si soy consultor?</a>
                                <div id="accordion-4" class="accordion-section-content">
                                    <p  class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Ingresa desde la opción "Ingresar como cliente" con tus mismos datos o realiza una busqueda directamente desde el "Home Kubera"</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice" class="accordion-section-title" href="#accordion-5">¿Cuánto cuesta ser parte de la comunidad?</a>
                                <div id="accordion-5" class="accordion-section-content">
                                    <p  class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Unirte a Kubera es GRATIS.  Puedes ser cliente, consultor o ambas sin costo alguno.  Solo pagas al contratar los servicios de un consultor.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                            </div><!--fin de  .accordion-section-content-->
                        </div><!--fin de  .accordion-section-->
                    </div><!--fin de  .accordion-->
                    
                    <div class="accordion">
                        <div class="accordion-section">
                            <a class="accordion-section-title" id="accordion6" href="#accordion-6">Cómo Administrar mi Cuenta y Perfil</a>
                            <div style="margin-left:56px" id="accordion-6" class="accordion-section-content">
                            <hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ></hr>
                                <a  id="subindice2" class="accordion-section-title" href="#accordion-7">Olvide mi usuario, ¿Cómo lo recupero?</a>
                                <div id="accordion-7" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Para tu facilidad, el nombre de usuario que te permite ingresar a Kubera es tu dirección de correo electrónico. Debes usar la misma dirección que ingresaste al momento del registro.  Intenta con la que recuerdas y te dejaremos saber si tu cuenta está creada con ella o no.  También puedes comunicarte a soporte@kubera.co o contactarnos a través del chat de servicio para ayudarte.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-8">Olvide mi contraseña, ¿Cómo la recupero?</a>
                                <div id="accordion-8" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Si no recuerdas tu contraseña, debes ingresar a la sección de inicio de sesión, y debajo de los campos de usuario y contraseña, dar click en la opción de "Olvide mi Contraseña". El sistema te solicitará la dirección de correo electrónica que usas para ingresar a Kubera. Te enviaremos un link y las instrucciones necesarias para crear una nueva contraseña.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-9">Kubera me dice que mi usuario está bloqueado. ¿Qué significa?</a>
                                <div id="accordion-9" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Si tu usuario se encuentra bloqueado, debes comunicarte a servicio al cliente a <a href="mailto:soporte@kubera.co">soporte@kubera.co</a> o a nuestro chat de servicio y te ayudaremos.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-10">¿Puedo actualizar mi perfil de cliente?</a>
                                <div id="accordion-10" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Sí, puedes cambiar tu "Nombre en Kubera", tu foto, tu "Ubicación" y tu número celular cuantas veces lo necesitas.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-11">¿Puedo cancelar mi cuenta?</a>
                                <div id="accordion-11" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Recuerda que tener una cuenta en Kubera no tiene costo alguno.  Sin embargo, si ya no quieres tener una cuenta en Kubera escríbenos a <a href="mailto:soporte@kubera.co">soporte@kubera.co</a> o a nuestro chat de servicio al cliente para ayudarte.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-12">¿Puedo tener varias cuentas en Kubera?</a>
                                <div id="accordion-12" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Es mejor que tengas una sola cuenta y así lo establecen los Términos y Condiciones de la Comunidad. De esta manera podrás tener información consolidada de toda tu actividad en Kubera.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-13">¿Puedo cambiar mi usuario en Kubera?</a>
                                <div id="accordion-13" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Tu usuario en Kubera es la forma única de identificarte en la comunidad.  Si por algún motivo ya no puedes acceder al correo con el que te registraste o necesitas cambiarlo, por favor comunícate a <a href="mailto:soporte@kubera.co">soporte@kubera.co</a> o escríbenos al chat de servicio para ayudarte.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-14">¿Qué pasa si no salí de mi cuenta, se queda conectada?</a>
                                <div id="accordion-14" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Si, quedarás conectado hasta que cierres tu navegador o apagues tu computador.</center></hr></font></p>  
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                            </div><!--fin de  .accordion-section-content-->
                        </div><!--fin de  .accordion-section-->
                    </div><!--Fin de acordeon-->

                    <div class="accordion">
                        <div class="accordion-section">
                            <a class="accordion-section-title" id="accordion15" href="#accordion-15">Cómo Encontrar Soluciones</a>
                            <div style="margin-left:56px" id="accordion-15" class="accordion-section-content">
                            <hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ></hr>
                                <a  id="subindice2" class="accordion-section-title" href="#accordion-16">¿Cómo encuentro los servicios que quiero?</a>
                                <div id="accordion-16" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Puedes usar los buscadores para encontrar anuncios de los expertos sobre el tema de tu consulta. Por ejemplo, si quieres asesoría financiera para declaración de impuestos, puedes buscar “declaración de impuestos” y te mostraremos todos los anuncios relacionados con los parámetros de <a href="http://www.kubera.co">búsqueda que ingresaste</a>. También puedes navegar por nuestras categorías y temas, descubriendo todo lo que la <a href="http://www.kurema.co">comunidad de expertos tiene para ofrecerte</a>. Otra opción es a través de los anuncios destacados donde podrás ver las <a href="http://www.kubera.co">recomendaciones de la comunidad, los más buscados, los últimos anuncios publicados, los mejores consultores, etc.</a></center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-17">¿Cómo contrato un servicio?</a>
                                <div id="accordion-17" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Una vez que eres miembro de la comunidad, puedes contratar los servicios de los consultores en 3 sencillos pasos:  1. Los consultores han creado anuncios y perfiles para que te resulte fácil encontrarlos 2. Mira las reseñas, analiza el precio y condiciones del servicio. Contáctalo inmediatamente cuando esté disponible luego de un secillo proceso de pago.  3. Haz una consulta precisa a tu experto y explícale bien tus expectativas. Al finalizar la sesión califica tu experiencia.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-18">Encuentro varios anuncios de un mismo consultor y a diferentes precios ¿Por qué? </a>
                                <div id="accordion-18" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Un mismo consultor puede publicar diferentes anuncios.  Los anuncios varían en los temas específicos que tratan así como en sus condiciones: requisitos, duración, precio.  Te recomendamos ver los detalles de cada uno para que hagas una buena elección.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-19">Veo anuncios similares, pero ubicados en diferentes s categorías o temas. ¿Cómo se cuál debo elegir?</a>
                                <div id="accordion-19" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">La comunidad Kubera te ofrece servicios orientados a tí, tu familia y hogar o a tu negocio, empresa o emprendimiento. Dependiendo el enfoque que tenga la solución que necesitas, puedes seleccionar el anuncio. Hay anuncios que están ubicados en varias categorías o temas para facilitar su búsqueda.  Fíjate en los detalles del mismo para asegurar que estás contratando la sesión que más se adapta a tu necesidad.</center></hr></font></p>    
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-20">¿Qué hago si no encuentro los servicios que busco?</a>
                                <div id="accordion-20" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Kubera se va construyendo contigo y todos los miembros de la comunidad.  Si ahora no tenemos expertos prestando los servicios que buscas, puedes registrar tu necesidad en el siguiente link: (formulario). También puedes escribirnos a soporte@kubera.co o en nuetras redes sociales, <a href="https://www.facebook.com/KuberaWeb/">Facebook: www.facebook.com/KuberaWeb</a>, <a href="https://twitter.com/kuberaweb">Twitter: @kuberaweb</a>, <a href="https://www.linkedin.com/company/7167916?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A7167916%2Cidx%3A2-2-9%2CtarId%3A1465593687051%2Ctas%3Akubera">Linkedin</a>. Haremos lo posible por encontrarlos para tí y seguir sumando conocimiento a la comunidad.</center></hr></font></p>   
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                            </div><!--fin de  .accordion-section-content-->
                        </div><!--fin de  .accordion-section-->
                    </div><!--Fin de acordeon-->
                    
                    <div class="accordion">
                        <div class="accordion-section">
                        <a class="accordion-section-title" id="accordion21" href="#accordion-21">Acerca de los anuncios y la contratación de servicios</a>
                        <div style="margin-left:56px" id="accordion-21" class="accordion-section-content">
                            <hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ></hr>
                                <a  id="subindice2" class="accordion-section-title" href="#accordion-22">¿Qué es un anuncio?</a>
                                <div id="accordion-22" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Es una publicación con el detalle  de los servicios que un consultor te ofrece.  Un mismo consultor puede tener varios anuncios. Elije el que cubre tu necesidad.  En el anuncio puedes ver la duración de la sesión, su contenido, condiciones y precio. También la calificación y comentarios recibidos por otros miembros de la comunidad.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-23">Ya encontré el anuncio perfecto para mí, ¿qué debo hacer ahora?</a>
                                <div id="accordion-23" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Dentro de esta pantalla existe un botón que te permite contratar el servicio en ese momento si el consultor se encuentra disponible (captura de pantalla OnLine-Consultame Ahora).  Una vez pulsado el botón, tendrás la posibilidad de enviar un requerimiento más detallado a tu consultor vía chat o de contratar el servicio directamente. Si decides contratar, se iniciará el proceso de pago y luego podrás iniciar la sesión de video conferencia en vivo con tu experto.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-24">¿Qué significa cuando un anuncio dice uso de audífonos con micrófono?</a>
                                <div id="accordion-24" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Significa que debes poseer y habilitar audífonos y micrófono  en tu computadora ya que el consultor necesita interactuar contigo de manera privada o para mejor apreciación del audio.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                    
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-25">¿Qué significa cuando un anuncio dice necesita cámara encendida?</a>
                                <div id="accordion-25" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Significa que debes tener una camara de video instalada y habilitada en tu computadora ya que el consultor necesita interactuar contigo de manera virtual a través de video conferencia</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                    
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-26">Veo consultores o anuncio en diferentes estados: OnLine, Ocupado, OffLine. ¿Qué significan? ¿Cuándo los puedo contratar?</a>
                                <div id="accordion-26" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Un consultor puede estar en distintos estados dependiendo su disponibilidad en ese momento. OnLine: el consultor está conectado y disponible para recibir consultas en ese momento.  Puedes contratarlo y te atenderá de inmediato. Ocupado: el consultor se encuentra atendiendo una consulta en ese momento.  Podrás contratarlo tan pronto termine. Offline: el consultor está desconectado en ese momento. Puedes enviarle un mensaje y dejarle saber que lo estás buscando. También puedes revisar en las condiciones del anuncio los horarios en que es más probable que se conecte para contratarlo.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                    
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-27">Quisiera agendar una cita para otra fecha. ¿Es posible?</a>
                                <div id="accordion-27" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Si será posible en nuestro próxima versión, sabemos que esto es importante para tí.  Estamos a pocos días…</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                    
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-28">¿Puedo contactarme con el consultor antes de contratar la consultoría?</a>
                                <div id="accordion-28" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Lo puedes hacer, hemos preparado un chat para que puedas hacer preguntas generales sobre su preparacion, conocimientos y experiencia en general y asi puedas tomar la mejor decision al momento de contratarlo. Ten en cuenta que este chat es simplemente para analizar el perfil y no para realizar consultas que contemplen la consulta,  ni tampoco para compartir datos de contacto</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                    
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-29">¿Puedo contratar una tutoría o servicio para mi hijo menor de edad?</a>
                                <div id="accordion-29" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Claro que sí, bajo tu tutela y responsabilidad.  Tienes que hacerlo desde tu cuenta pues según los <a href="http://kubera.co/terminos-condiciones/">Términos y Condiciones</a> de la Comunidad un menor de edad no puede contratar servicios.</center></hr></font></p>  
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-30">Estoy tratando de contratar a un consultor pero no me permite realizar el pago. ¿Qué debo hacer?</a>
                                <div id="accordion-30" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Debes contactar a nuestro chat de soporte o escribir a la direccion <a href="mailto:soporte@kubera.co">soporte@kubera.co</a></center></hr></font></p>  
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                            </div><!--fin de  .accordion-section-content-->
                        </div><!--fin de  .accordion-section-->
                    </div><!--Fin de acordeon-->

                    <div class="accordion">
                        <div class="accordion-section">
                        <a class="accordion-section-title" id="accordion31" href="#accordion-31">Sobre las Sesiones con el Consultor y Cómo Prepararte Para Ellas</a>
                        <div style="margin-left:56px" id="accordion-31" class="accordion-section-content">
                            <hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ></hr>
                                <a  id="subindice2" class="accordion-section-title" href="#accordion-32">¿Cuánto dura una sesión?</a>
                                <div id="accordion-32" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Cada anuncio de servicios tiene publicado su "Tiempo de Sesión"en minutos.  Este tiempo corresponde al máximo que podría durar una consulta con el experto que contrataste, sin embargo podría durar menos si estás satisfecho con la solución recibida sin necesidad de esperar hasta el final.   Como mínimo los consultores programan sesiones de 15 minutos.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-33">¿En que idiomas se lleva a cabo la sesión?</a>
                                <div id="accordion-33" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">El idioma primario de Kubera es el español , sin embargo, y dependiendo del perfil del consultor , de sus conocimientos y su disponibilidad; la consultoria se puede llevar a acabo en otro idioma. Por ello te sugerimos que revises todos los detalles del consultor antes de contratar sus servicios</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-34">¿Puede un consultor venir a mi casa o domicilio?</a>
                                <div id="accordion-34" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">La plataforma ha sido creada para que puedas recibir tu consultoria en tu hogar u oficina sin la molestia de cambiar de ubicación. Ademas tu seguridad nos importa y aunque hemos desarrrollado un filtro de seguridad y excelencia para los consultores Kubera , te sugerimos no establezcas una relacion mas alla de la profesional y dentro de la plataforma. Nuestra politica senala que el unico espacio para transaccionar entre consultores y clientes es dentro de la plataforma.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                    
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-35">¿Puedo contratar a alguien para otros servicios que no sean prestados a través de la video conferencia y/o la plataforma Kubera?</a>
                                <div id="accordion-35" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Kubera está para la conexión entre expertos y buscadores.  Su propósito es encontrar soluciones directas en vivo a través de video conferencia o video chat.  Si tú haz hecho una relación con tu consultor por Kubera y ves que pueden tener una relación de negocios posterior en la que necesitan ampliar el alcance de los servicios y este no está cubierto en Kubera, eres libre de llegar a acuerdos con él.  La comunidad no interviene ni avala dichos servicios.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                    
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-36">¿Qué debo hacer antes de una sesión?</a>
                                <div id="accordion-36" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">En el detalle de la sesión que contrataste te da las instrucciones sobre conocimientos previos y prerequisitos que aplican para esa particular sesión.  De igual forma tienes las restricciones y limitaciones.  Asegúrate que has considerado todo lo solicitado por tu experto antes de iniciar la sesión.  En cuanto a la parte técnica, debes cumplir con los mínimos requisitos establecidos: <a href="../faqkubera/accordion-48"></a>Requisitos técnicos</a>.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                    
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-37">¿Qué requisitos tengo para poder conectarme a la Video Conferencia?</a>
                                <div id="accordion-37" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Debes entrar desde tu laptop o computador. Para realizar la sesión ONLIVE, requieres Adobe Flash Player. Si no lo tienes instalado, instálelo en el enlace siguiente: <a href="http://get.adobe.com/es/flashplayer/">http://get.adobe.com/es/flashplayer/</a>. Requieres una conexión a internet de al menso 512 Kbps.  tu computador debe tener una cámara, parlantes y micrófonos o mejor aún si usas audífonos con micrófono durante la sesión.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                    
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-38">¿Puedo hacer una video conferencia desde mi móvil o tablet?</a>
                                <div id="accordion-38" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Estamos próximos a salir con las aplicaciones móviles de Android y IOS, pronto lo podrás hacer.  Ahora puedes utilizar un computadora de escritorio o laptop.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                    
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-39">¿Es necesario descargarme algún sistema paa hacer la video conferencia?</a>
                                <div id="accordion-39" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">No requieres sistemas adicionales, en el mismo navegador se abrirá una venta para dar inicio a tu sesión.  Para que la misma funcione mejor, necesitas Adobe Flash Player.  Si no lo tienes instalado, instálelo en el enlace siguiente: <a href="http://get.adobe.com/es/flashplayer/">http://get.adobe.com/es/flashplayer/</a> .</center></hr></font></p>  
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                            </div><!--fin de  .accordion-section-content-->
                        </div><!--fin de  .accordion-section-->
                    </div><!--fin de  .accordion-->

                    <div class="accordion">
                        <div class="accordion-section">
                            <a class="accordion-section-title" id="accordion40" href="#accordion-40">Como realizar los pagos y que costo tiene el uso de la plataforma</a>
                            <div style="margin-left:56px" id="accordion-40" class="accordion-section-content">
                                <hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ></hr>
                                <a  id="subindice2" class="accordion-section-title" href="#accordion-41">¿Cuánto cuesta una sesión?</a>
                                <div id="accordion-41" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">El costo de cada sesión depende del servicio que contrataste.  El valor lo determina el consultor y está detallado en cada anuncio de servicios.  Adicional a eso aplicamos los impuestos de ley correspondientes dependiendo de dónde estás ubicado y dónde está ubicado el consultor que te prestará el servicio.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-42">¿Qué impuestos aplican al servicio?</a>
                                <div id="accordion-42" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">El impuesto aplicable depende de dónde estás tú y dónde está el consultor sleccionado</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-43">¿Qué medios de pago son aceptados en Kubera?</a>
                                <div id="accordion-43" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Aceptamos todas las tarjetas de crédito de las marcas internacionales más conocidas.  Dependiendo tu país de residencia, también aceptamos transferencias bancarias.  En Colombia por ejemplo a través de la red PSE.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-44">¿En qué moneda me van a cobrar?</a>
                                <div id="accordion-44" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Kubera trabaja con dólares de los Estados Unidos como moneda principal.  A través de la procesadora de pagos se realiza la conversión a tu moneda local.  Por ejemplo, si estás en Colombia, se realiza la conversión a pesos colombianos en la fecha que registra la transacción y según la tasa oficial que aplica tu medio de pago. </center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-45">¿Es seguro pagar a traves de Kubera?</a>
                                <div id="accordion-45" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Kubera cuenta con todas las seguridades necesarias para que realices tu transacción. Para aumentar aún más el nivel de seguridad, no guardamos información de tu cuenta o tarjeta de crédito en nuestro sistema. Por ejemplo, si usas débito a tu cuenta bancaria, podrás observar que el sistema te llevará a la página de Banca En Línea de tu institución financiera, y será en ese portal en el que podrás completar la transacción. Las procesadoras de pago utilizadas cumplen con las normativas vigentes.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-46">¿Cómo recibo la factura de servicios?</a>
                                <div id="accordion-46" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Inmediatamente y luego de terminar tu consulta con el consultor se te enviara un recibo por el costo del a transacción mas  impuestos. Máximo en 3 dias laborables recibirás una factura a tu correo electrónico registrado. Esta factura es legal y puede servirte como soporte contable y tributario.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-47">¿Qué medios de pago son aceptados en Kubera?</a>
                                <div id="accordion-47" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Aceptamos todas las tarjetas de crédito de las marcas internacionales más conocidas.  Dependiendo tu país de residencia, también aceptamos transferencias bancarias.  En Colombia por ejemplo a través de la red PSE.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-48">¿Qué hago para cancelar una sesión una vez que ya he realizado el pago?</a>
                                <div id="accordion-48" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Al iniciar el proceso de contratación el consultor ya está reservado para ti y esperando que te conectes a la sesión.  Entendemos que hay motivos por los cuales a último momento puedes cambiar de opinión. Cancela la sesión e inmediatamete acreditaremos el valor pagado a tu cuenta en Kubera (Kubera Credits).  Este valor podrás utilizarlo más adelante para contratar otra sesión con el mismo o con otro consultor. </center></hr></font></p>  
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-49">¿A quien puedo acudir si tengo algún problema en el pago?</a>
                                <div id="accordion-49" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Si tienes algun problema con el pago puedes acudir a nuestro chat virtual de soporte o escribir al email <a href="mailto:soporte@kubera.co">soporte@kubera.co</a>.</center></hr></font></p>  
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-50">¿Qué es un Kubera Credit?</a>
                                <div id="accordion-50" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Los créditos Kubera son saldos a favor que tienes en tu cuenta y que pueden ser utilizados para pagar todo o parte de sesiones en Kubera en cualquier momento. </center></hr></font></p>  
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-51">¿Puedo utilizar un Kubera Credit para pagar por una sesión?</a>
                                <div id="accordion-51" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Sí, puedes utilizar en cualquier momento los Kubera Credits para pagar por una sesión de cualquier consultor en forma total o parcial, tú decides.</center></hr></font></p>  
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-52">¿Qué hago si no he recibido un recibo de pago?</a>
                                <div id="accordion-52" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Verifica tu correo electrónico.  Te recomendamos que chequees en la bandeja de correos indesados por si el mismo fue derivado para allá.  Si no lo recibiste, por favor comunícate con nosotros en el chat de servicio al cliente o envíanos un correo a <a href="mailto:soporte@kubera.co">soporte@kubera.co</a></center></hr></font></p>  
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-53">¿Qué hago cuando se produce un error al momento del pago?</a>
                                <div id="accordion-53" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Puedes comunicarte con nosotros a <a href="mailto:soporte@kubera.co">soporte@kubera.co</a> o a nuestro chat de servicio para entender de qué se trata el problema y ayudarte con la mejor solución.</center></hr></font></p>  
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-54">¿Por qué una transacción de pago puede salir rechazada?</a>
                                <div id="accordion-54" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Normalmente puede tratarse de algún inconveniente con tu medio de pago.  Puede ser una falla temporal en conexión, cupo disponible, fecha de caducidad, prevención, problemas de pago, entre algunos de los motivos más comunes.  Tú conoces bien a tu medio de pago.  Puedes volver a intentar una vez más, cambiar a otro medio de pago o  comunicarte con tu medio de pago para averiguar el motivo de rechazo de la transacción.</center></hr></font></p>  
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-55">¿Por qué el valor que me cobraron es distinto al valor que estaba en el anunciado del servicio?</a>
                                <div id="accordion-55" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Kubera trabaja con dólares de los Estados Unidos como moneda principal.  Te mostramos un precio referencial en tu moneda local para que los valores te sean más familiares. Este precio referencial no corresponde necesariamente al precio final que se aplicará.  Este depende del tipo de cambio que aplica la procesadora de pago al momento de la transacción y según tu medio de pago.  Si tienes más dudas, podemos ayudarte en el chat de servicio o si nos mandas un mail a <a href="mailto:soporte@kubera.co">soporte@kubera.co</a>.</center></hr></font></p>  
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-56">¿Qué pasa si se presentó algún problema con mi tarjeta de crédito posterior a recibido el servicio?</a>
                                <div id="accordion-56" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Puedes comunicarte con nosotros a soporte@kubera.co o a nuestro chat de servicio para entender de qué se trata el problema y ayudarte con la mejor solución</center></hr></font></p>  
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                            </div><!--fin de  .accordion-section-content-->
                        </div><!--fin de  .accordion-section-->
                    </div><!--fin de  .accordion-->

                    <div class="accordion">
                        <div class="accordion-section">
                            <a class="accordion-section-title" id="accordion57" href="#accordion-57">Qué Hacer Durante la Video Conferencia</a>
                            <div style="margin-left:56px" id="accordion-57" class="accordion-section-content">
                                <hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ></hr>
                                <a  id="subindice2" class="accordion-section-title" href="#accordion-58">¿Qué hago cuando soy la única persona en la sala?</a>
                                <div id="accordion-58" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Tu consultor está conectándose en este momento.  Dale unos minutos y entrará a la sala para iniciar tu sesión.  Si esto no sucede dentro de un timepo prudente de espera, puedes comunicarte con nosotros a <a href="mailto:soporte@kubera.co">soporte@kubera.co</a> o a nuestro chat de servicios.  Para más información sobre el funcionamiento de la herramienta de video conferencia, visita los Tutoriales en el Centro de Ayuda.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-59">Al entrar a la sala me pide elegir  cómo manejar el audio, "micrófono" o "solo escuchar", ¿Cómo funciona y qué debo elegir?</a>
                                <div id="accordion-59" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Este es una pequeña prueba para asegurar que el consultor pueda escucharte.  Selecciona la opción de micrófono.  Sigue las instrucciones de la prueba y presiona siguiente.  Para más información sobre el funcionamiento de la herramienta de video conferencia, visita los Tutoriales en el Centro de Ayuda</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-60">¿Sale un mensaje de que ha fallado el audio y que intente con Flash, ¿qué debo hacer?</a>
                                <div id="accordion-60" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Por favor acepta la opción para que el sistema utilice Flash y puedas escuchar adecuadamente.  Si no tienes Flash instalado, puedes hacerlo a través de este link: <a href="http://get.adobe.com/es/flashplayer/">http://get.adobe.com/es/flashplayer/</a>. Para más información sobre el funcionamiento de la herramienta de video conferencia, visita los Tutoriales en el Centro de Ayuda</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-61">¿Para qué sirve la prueba de audio? ¿Qué hago si no puedo escuchar?</a>
                                <div id="accordion-61" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Sirve para asegurar que puedes escuchar a tu consultor.  Por favor habla unas pocas palabras y confirma si escuchas tu voz.  De ser así, presiona si y continuar.  En caso de no escuchar el audio por favor sube el volúmen de los parlantes de tu computador.  Si estás con auriculares, comprueba que los mismos están bien conectados y funcionando.  No olvides que los mismos deben tener micrófono incorporado.  También puedes probar los parlantes desde esta opción.  Presiónala y asegúrate que escuchas la música a buen volumen y de forma clara.  Para más información sobre el funcionamiento de la herramienta de video conferencia, visita los Tutoriales en el Centro de Ayuda.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-62">El consultor no puede verme, ¿qué debo hacer?</a>
                                <div id="accordion-62" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Es necesario que enciendas tu cámara web.  Puedes hacerlo en la esquina superior izquierda de la pantalla, en el ìcono al costado del micrófono.  Por favor presiónalo.  Te saldrá un pop-up donde podrás verte.  Ajusta la resolución de la cámara y presiona el botón Aceptar.  Verás que ahora apareces en un recuadro en la pantalla.  Para más información sobre el funcionamiento de la herramienta de video conferencia, visita los Tutoriales en el Centro de Ayuda.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-63">Veo muy pequeña la imagen del consultor, ¿Cómo la hago más grande?</a>
                                <div id="accordion-63" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">En la esquina inferior derecha encontrarás un botón con opciones de vista de pantalla.  Si quieres tener al consultor en pantalla completa te recomendamos utilizar la vista de "Video Chat".  Para más información sobre el funcionamiento de la herramienta de video conferencia, visita los Tutoriales en el Centro de Ayuda.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-64">No puedo escuchar al consultor, ¿Qué debo hacer?</a>
                                <div id="accordion-64" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Déjale saber que no puedes orile.  Puedes utilizar la heramienta de chat al costado derecho para escribirle y que el ajuste sus configuraciones.  Para más información sobre el funcionamiento de la herramienta de video conferencia, visita los Tutoriales en el Centro de Ayuda.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-65">Mi conexión de internet falló y salé del sistema, ¿Cómo regreso a la sesión?</a>
                                <div id="accordion-65" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">A penas contrates la sesión enviamos un link a tu mail registrado.  Abre el correo e ingresa a través del link, volverás a conectarte directamente a la sesión con tu consultor. Para más información sobre el funcionamiento de la herramienta de video conferencia, visita los Tutoriales en el Centro de Ayuda.</center></hr></font></p>  
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-66">¿Puedo descargar la presentación que el consultor me esnseñó?</a>
                                <div id="accordion-66" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Sí, puedes descargar la presentación.  Al costado de inferior izquierdo de la misma hay un botón que te permite descargar. Para más información sobre el funcionamiento de la herramienta de video conferencia, visita los Tutoriales en el Centro de Ayuda.</center></hr></font></p>  
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-67">¿Puedo descargar la presentación que el consultor me esnseñó?</a>
                                <div id="accordion-67" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Sí, puedes descargar la presentación.  Al costado de inferior izquierdo de la misma hay un botón que te permite descargar. Para más información sobre el funcionamiento de la herramienta de video conferencia, visita los Tutoriales en el Centro de Ayuda.</center></hr></font></p>  
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-68">¿Cómo hago para mostrarle al consultor un documento en la pantalla de mi computador?</a>
                                <div id="accordion-68" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Pídele al consultor que te dé el comando de la sesión.  Una vez que lo haya hecho, presiona el ícono de pantalla que tienes en la esquina superior izquierda.  Cuando quieras dejar de compartir tu pantalla, vuelve a presionar dicho ícono.  Para devolver el control de la presentación al consultor puedes ir a la "vista default" (esquina inferior derecha) y hacerlo desde la ventana que aparece con el nombre del consultor arriba a la izquierda.  Para más información sobre el funcionamiento de la herramienta de video conferencia, visita los Tutoriales en el Centro de Ayuda.</center></hr></font></p>  
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-69">¿Qué pasa si se termina el tiempo de sesión?</a>
                                <div id="accordion-69" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Tu consultor configuró un tiempo máximo de sesión en su anuncio de servicios.  El sistema reservó el espacio de video conferencia para este período.   Si el tiempo máximo concluye la sesión terminará.  Te enviaremos notificaciones antes para que tengas referencia del tiempo y puedas administrarlo de la mejor manera.  Si necesitas tiempo adicional con tu consultor puedes volverlo a contratar.</center></hr></font></p>  
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                            </div><!--fin de  .accordion-section-content-->
                        </div><!--fin de  .accordion-section-->
                    </div><!--fin de  .accordion-->

                    <div class="accordion">
                        <div class="accordion-section">
                            <a class="accordion-section-title" id="accordion70" href="#accordion-70">Calificaciones, Reseñas y Favoritos</a>
                            <div style="margin-left:56px" id="accordion-70" class="accordion-section-content">
                                <hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ></hr>
                                <a  id="subindice2" class="accordion-section-title" href="#accordion-71">¿Para qué sirven las calificaciones y reseñas?</a>
                                <div id="accordion-71" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Este es el mecanismo de la comunidad para entender cómo van los servicios de Kubera, los servicios que te presta el consultor y la calidad del contenido que recibiste.  Al calificarnos luego de terminada una sesión te agradecemos que seas lo más objetivo posible de esa manera nos ayudarás a mejorar y la comunidad podrá tomar mejores decisiones al contratar servicios.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-72">¿Qué significan las estrellas?</a>
                                <div id="accordion-72" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Representan la calificación que das a los diferentes servicios siendo 5 estrellas la máxima calificación y 1 estrella la mínima.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-73">¿Es posible dejar un comentario sin que el mismo se publique?</a>
                                <div id="accordion-73" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Sí, al momento de calificar tienes un recuadro con un "check" que podrás quitar en caso de que no quieras que tu comentario sea público pero sí quieras que tu reseña nos llegue a nosotros y a tu consultor.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-74">¿Dónde puedo ver lo que otros usuarios han comentado sobre un consultor o su anuncio?</a>
                                <div id="accordion-74" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Ingresa al anuncio que estás interesado en contratar y elige la pestaña de comentarios.  Ahí podrás ver lo que otros buscadores han comentado públicamente sobre una anuncio de servicios.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-75">¿Puedo dejar una reseña a un consultor si no he contratado sus servicios?</a>
                                <div id="accordion-75" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Esto no es posible pues queremos ser muy objetivos y calificar al consultor en relación a los servicios que el presta en Kubera.  Únicamente los clientes que han realizado sesiones podrán calificarlos.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-76">Escribí una reseña o comentario de un consultor pero el mismo no está publicado. ¿Por qué?</a>
                                <div id="accordion-76" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Los comentarios son revisados antes de publicarse para asegurar que los mismos no van en contra del <a href="http://kubera.co/codigo-etica/">Código de Ética y Conducta</a> de la comunidad así como de los <a href="http://kubera.co/terminos-condiciones/">Términos y Condiciones</a>. Revisa tu correo electrónico para saber cuál es el estado de tu comentario.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                            </div><!--fin de  .accordion-section-content-->
                        </div><!--fin de  .accordion-section-->
                    </div><!--fin de  .accordion-->

                    <div class="accordion">
                        <div class="accordion-section">
                            <a class="accordion-section-title" id="accordion77" href="#accordion-77">Reclamos y Devoluciones</a>
                            <div style="margin-left:56px" id="accordion-77" class="accordion-section-content">
                                <hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ></hr>
                                <a  id="subindice2" class="accordion-section-title" href="#accordion-78">El consultor que contraté nunca se conecto a la sala. ¿Cómo me devuelven mi dinero?</a>
                                <div id="accordion-78" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Lamentamos mucho este hecho.  Los consultores asumen una responsabilidad directa con la comunidad al momento de aceptar prestar sus servicios en Kubera.  Trabajamos constantemente con los consultores para promover que buscadores como tú tengan la mejor experiencia.  Inmediatamente acreditaremos el valor de tu sesión a tu cuenta en Kubera.  Este valor podrás utilizarlo para sesiones de cualquier servicio en la comunidad. En caso de que no hayas recibido este crédito automático el cual puedes verificarlo en tu "DashBoard" al ingresar a tu cuenta, por favor escríbenos <a href="mailto:soporte@kubera.co">soporte@kubera.co</a> o a través de nuestro chat de servicio.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-79">El consultor se desconectó durante la sesión antes de que esta concluya. ¿Qué debo hacer?</a>
                                <div id="accordion-79" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Es posible que tu consultor haya experimentado un problema de conexión (acceso a internet, energía, etc.).  Lamentamos que hayas tenido esa experiencia.  Es algo que puede suceder y está fuera del alcance de lo que podemos manejar.  Trabajamos constantemente para que los consultores tengan disponibles las mejores conexiones posibles para tener una buena experiencia con sus cleintes.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                            </div><!--fin de  .accordion-section-content-->
                        </div><!--fin de  .accordion-section-->
                    </div><!--fin de  .accordion-->

                    <div class="accordion">
                        <div class="accordion-section">
                            <a class="accordion-section-title" id="accordion80" href="#accordion-80">Mediacion entre Consultores y Clientes</a>
                            <div style="margin-left:56px" id="accordion-80" class="accordion-section-content">
                                <hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ></hr>
                                <a  id="subindice2" class="accordion-section-title" href="#accordion-81">Puedo realizar una queja o un reclamo de un consultor?</a>
                                <div id="accordion-81" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Si lo puedes hacer a travez del chat de servicio o escribiendo a la dirección <a href="mailto:soporte@kubera.com">soporte@kubera.com</a>. Atenderemos tu caso lo antes posible.   Queremos que te vaya bien y haremos lo posible por que tu experiencia en Kubera sea la mejor.  Nos preocupamos por el desarrollo y mejoramiento de los consultores que prestan sus servicios en la comunidad Kubera.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                            </div><!--fin de  .accordion-section-content-->
                        </div><!--fin de  .accordion-section-->
                    </div><!--fin de  .accordion-->
   
                    </div>
                    <br>
                    <hr id="hr2" style="width: 68%;margin-left: 177px;">
                    <button id="volver3" class="btn btn-lg" onClick="volver();"><i class="fa fa-chevron-left"></i> Volver</button>
                    <button id="volver3" class="btn btn-lg" onClick="ayuda();">Ayuda como consultor <i class="fa fa-chevron-right"></i></button>
                </div><br>




                <div class="col-md-12">
                    <div class="col-md-6 text-center tutoriales2">
                        <a class="cafe" href="">Videos y Tutoriales</a>
                    </div>
                    <div class="col-md-6 text-center blog2">
                        <a class="cafe" href="">Blog</a>
                        <br>
                    
                </div><br>
                <div style="margin-left:30px" class="col-md-6">
                    <a href="#"><font color="#A4A4A4" size=3><DIV ALIGN=center>
                    <br>
                    <em><u id="codigo" style="margin-left: 4px;"><a id="link2" href="http://kubera.co/codigo-etica/">Código de conducta</a></u></em></DIV></font></a>
                </div>
                 <div class="col-md-4">
                    <a href="#"><font color="#A4A4A4" size=3><DIV ALIGN=center>
                     <br>       
                    <em><u id="terminos" style="margin-left: -34px;"> <a id="link2" href="http://kubera.co/terminos-condiciones/">Términos, Condiciones y Políticas de Privacidad</a></u></em></DIV></font></a>
                </div></div></div>
                <!--<div class="col-md-4">
                    AQUI CHAT
                </div>-->
           
        </div></div><br>
        <!-- Termina el Contenedor de la pagina -->
        <!-- Comienza el Footer -->
       <footer>
        <div class="wrapper">
            <div class="footer col-md-12">
                <div class="col-md-6 ayuda">
                    <img src="assets/img/logo_kubera_amarillo.png"><br><br>
                    <p class="amarillo">¡Queremos que te vaya bien!</p><br><br>
                    <a class="btn boton-unete btn-lg" href="#"> Únete gratis ahora<i class="fa fa-chevron-right"></i></a><br><br>
                    <p class="blanco">¿Necesitas ayuda?</p>
                    <p class="amarillo">soporte@kubera.co</p>
                    <!--<p class="amarillo">1 800 KUBERA</p>-->
                </div>
                <div class="col-md-3">
                <p id="textoPoder" class="blanco">Todos tenemos un SÚPER PODER, algo en lo que somos realmente buenos. Aquí puedes ganar dinero compartiendo ese conocimiento y encontrar SOLUCIONES directas a lo que estás buscando.</p>
                    <!--<a href=""></a>
                    <div id="">
                        <p class="amarillo">LA EMPRESA</p>
                        <p class="blanco">Acerca de</p>
                        <p class="blanco">Contacto</p>
                        <p class="blanco">Empleo</p>
                        <p class="blanco">Ayuda</p>
                        <p class="blanco">Blog</p>
                    </div>-->
                </div>
                <div class="col-md-3 servicio">
                     <a href=""></a>
                     <div id="">
                        <p class="amarillo">EL SERVICIO</p>
                        <p class="blanco">Categorías</p>
                        <!--<p class="blanco">Preguntas frecuentes</p>-->
                        <!--<p class="blanco">Planes y tarifas</p>-->
                        <p class="blanco"><a id="link" href="http://kubera.co/terminos-condiciones/">Terminos y condiciones</a></p>
                        <p class="blanco"><a id="link" href="http://kubera.co/codigo-etica/">Código de ética y conducta</a></p>
                    </div>
                </div>
                <div class="col-md-12 text-center redes">
                    <p class="amarillo">Siguenos en: </p>
                    <a href="#"><img src="assets/img/fb.png"></a>
                    <a href="#"><img src="assets/img/tw.png"></a>
                    <a href="#"><img src="assets/img/in.png"></a></br></br>
                    <p class="blanco">© Kubera, Inc.</p>
                </div>
            </div>
        </div></footer>
        </footer>
        <!-- Termina el Footer -->
        <!-- Boton back to top -->
        <a href="#" class="back-to-top" style="display: block;"><i class="fa fa-angle-up"></i></a>
    </body>
    <script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/js/jquery-ui.js"></script>
        <script type="text/javascript" src="assets/js/jquery.nicescroll.min.js"></script>
        <script type="text/javascript" src="assets/js/jquery.autocomplete.min.js"></script>
        <script type="text/javascript" src="assets/js/currency-autocomplete.js"></script>
        <script type="text/javascript" src="assets/js/funciones.js"></script>
    <script type="text/javascript">
    function mostrar(enla) {
      obj = document.getElementById('oculto');
      obj.style.display = (obj.style.display == 'block') ? 'none' : 'block';
      enla.innerHTML = (enla.innerHTML == 'COMENZANDO') ? 'COMENZANDO' : 'COMENZANDO';
    }
    </script>
    <!-- Termina el Body -->
    <script type='text/javascript' data-cfasync='false'>window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: 'ea6cfbb6-15d7-4d7b-be89-90cb5988fa97', f: true }); done = true; } }; })();</script>
</html>
