<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title>Centro de Ayuda</title>
        <link rel="shortcut icon" href="assets/img/favicon.ico" />
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:800,700,600,400' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="assets/css/estilos.css">
        <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
        <link rel="stylesheet" type="text/css" href="assets/css/acordion.css">
        <script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/js/jquery-ui.js"></script>
        <script type="text/javascript" src="assets/js/jquery.nicescroll.min.js"></script>
        <script type="text/javascript" src="assets/js/jquery.autocomplete.min.js"></script>
        <script type="text/javascript" src="assets/js/currency-autocomplete.js"></script>
        <script type="text/javascript" src="assets/js/funciones.js"></script>
    </head>
    <body>
        <!-- Comienza el Body -->
        <!-- Comienza Header -->
        <header class="navbar-header header">
            <div class="wrapper">
            <div class="logo" style="margin-top: 26px;"></div>
            <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-navbar" aria-controls="bs-navbar" aria-expanded="false"> 
            <span class="sr-only">Toggle navigation</span> 
            <span class="icon-bar"></span> 
            <span class="icon-bar"></span> 
            <span class="icon-bar"></span> 
            </button>
            <nav class="navbar-collapse collapse">
            <a id="buscar" class="amarillo" href="#">Buscar<i class="fa buscar-icon"></i></a>
            <a id="unirse" class="amarillo" href="#">Unirse<i class="fa unirse-icon"></i></a>
            <a id="ingresar" class="amarillo" href="#">Ingresar<i class="fa ingresar-icon"></i></a>
            </nav>
            </div>
            <nav style="height: 1px;" aria-expanded="false" id="bs-navbar" class="collapse"> 
            <ul class="nav navbar-nav" style="text-align: center;"> 
            <li class="active"><a href="#">Buscar<i class="fa buscar-icon"></i></a></li> 
            <li><a href="#">Unirse<i class="fa unirse-icon"></i></a></li>
            <li><a href="#">Ingresar<i class="fa ingresar-icon"></i></a></li> 
            </ul> 
            </nav>
        </header>
        <!-- Termina Header -->
        <!-- Comienza el contendor de la pagina -->
        <div class="container">     
            <div class="col-md-12">
            <br>
                <FONT COLOR="#6b3f23" id="ruta"><b><i class="fa fa-chevron-left"></i> Inicio / El Servicio / Cómo Funciona</b></FONT><br>
                <b><h2 style="font-size: 45px;font-weight: 700;margin-top: 20px;">Bienvenido al centro de ayuda</h2></b> 
                <FONT style="font-size: 30px;font-weight: normal;margin-left: 358px;" FACE="arial" SIZE=5 COLOR="#6b3f23" id="subtitulo">¡Nos gusta que te vaya bien!</FONT>
                <center><iframe style="margin-top: 10px;" width="750" height="400" src="http://www.youtube.com/embed/"></iframe></center>
                <div class="container buscar" style="width: 80%;">
                <form id="form" name="form" class="buscar-consultor" action="" style="position: relative;">
                    <div id="selection" class="blanco"  style="font-size: 16px; font-weight:600">¿En qué podemos ayudarte?</div><br>
                        <input id="mainSearcherBox" value="" name="" class="busqueda" type="text" placeholder="Escribe aquí el tema sobre el que necesitas más información" onkeydown="">
                        <button style="font-size: 14px;" class="empezar-busqueda" type="button" onclick="search(&#39;form&#39;);">Buscar</button>
                    <div id="unibox-suggest-box" style="min-width: 810px; max-width: 398px;"></div><div id="unibox-invisible"></div></form>
                </div>
                <div class="bottom-arrow"></div>
                <div class="col-md-12 text-center">
                   <center><div style="min-width: 310px; max-width: 198px;  margin-left:250px; margin-top:30px" id="clientes" class="col-md-7">
                        <img id="clientesImg" style="margin-left: -40px;" src="assets/img/ico_clientes.png">
                         <h3 style="margin-left: -40px;" id="h4Clientes" class="cafe">Clientes</h3>
                    </div>
                    </center> 
                    <div style="margin-top:30px" id="consultores" class="col-md-1">
                        <img id="consultor1" src="assets/img/ico_consultores.png">
                        <h3 style="margin-left: 58px;" id="h4Consultores" class="amarillo">Consultores</h3>
                    </div>
                </div>
                <hr id="hr1" style="width: 60%; margin-left: 220px;">
                <button  onclick="mostrar1(this); return false" value="Mostrar1" id="comenzandoCliente2" class="btn btn-lg" onClick="comenzandoCliente2();"><i class="fa fa-chevron-left"></i> COMENZANDO</button>
                 <a id="unirse" class="amarillo" href="#"><i class="fa unirse-icon-6"></i></a>
                
                <script type="text/javascript">
                function mostrar1(enla) {
                  obj = document.getElementById('oculto1');
                  obj.style.display = (obj.style.display == 'block') ? 'none' : 'block';
                  enla.innerHTML = (enla.innerHTML == 'COMENZANDO') ? 'COMENZANDO' : 'COMENZANDO';
                } 
                </script>


                <div style="margin-top:27px" class="text-center">

                <div id='oculto1' style="display:none";> 

                <div class="accordion">
                            <div class="accordion-section">
                                <a class="accordion-section-title active"  id="accordionA" href="#accordion-A">¿Qué es Kubera?</a>
                                <div id style="margin-left:56px" id="accordion-A" class="accordion-section-content">
                                <p class="respuesta"><font id="p1" color="#8A4B08"><hr style="width: 61%;margin-left: 174px;" size="2" color="#F2F2F2" ><center style="width: 62%;margin-left: 171px;text-align: justify;font-size: 18px;">Kubera es la comunidad del intercambio de conocimiento y el desarrollo personal. El punto de encuentro de los que buscan soluciones y los que escuchan, conocen y dan soluciones. De los que crecen y quieren ver a los demás crecer. Para ello, proveemos una plataforma para que los miembros de la comunidad, pongan su conocimiento al servicio de los demás, ofreciendo soluciones en vivo y en directo a través de una video conferencia en línea.</hr></font></p>
                                </center><hr  style="width: 61%;margin-right: 303px;" size="2" color="#F2F2F2" ></hr>
                                <br>
                                    <a id="subindice" class="accordion-section-title" href="#accordion-A1">¿Cómo lo hacemos?</a>
                                    <div id="accordion-A1" class="accordion-section-content">
                                        <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Ayudamos a los consultores a crear su perfil, a identificar sus talentos a crear sus cursos y promocionarlos. Usamos su experiencia para mejorar la plataforma. Conectamos a personas que buscan con los que ofrecen sus servicios dándole a ambos una excelente experiencia de uso.</center></hr></font></p>
                                        <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2"></hr>
                                    </div><!--fin de  .accordion-section-content-->
                                    <a id="subindice" class="accordion-section-title" href="#accordion-A2">¿Qué hacemos?</a>
                                    <div id="accordion-A2" class="accordion-section-content">
                                        <p  class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Proveemos una plataforma para que los miembros de la comunidad, pongan su conocimiento al servicio de los demás, ofreciendo soluciones en vivo y en directo. Les damos un canal de venta. Proveemos una excelente experiencia de usuario para buscadores y oferentes de servicios</center></hr></font></p>
                                        <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                    </div><!--fin de  .accordion-section-content-->
                                </div><!--fin de  .accordion-section-content-->
                            </div><!--fin de  .accordion-section-->
                        </div><!--fin de  .accordion-->
                        <div class="accordion">
                            <div class="accordion-section">
                                <a   class="accordion-section-title active" id="accordionB" href="#accordion-B">¿Por qué lo hacemos?</a>
                                <div style="margin-left:56px;" id="accordion-B" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ><center style="width: 62%;margin-left: 199px;text-align: justify;font-size: 18px;">Queremos que la gente crezca, descubra su talento y le vaya bien. Lideramos una nueva era del trabajo y los negocios impulsada por elintercambio de conocimiento, la generosidad y la curiosidad.</center></hr></font></p>
                                    <hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                            </div><!--fin de  .accordion-section-->
                        </div><!--fin de  .accordion-->
                        <div class="accordion">
                            <div class="accordion-section">
                                <a class="accordion-section-title active" id="accordionC" href="#accordion-C">¿Para quién lo hacemos?</a>
                                <div style="margin-left:56px; " id="accordion-C" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ><center style="width: 62%;margin-left: 199px;text-align: justify;font-size: 18px;">Para toda persona que comparten nuestros valores. Que creen como nosotros que al compartir conocmiento, todos crecemos. Kubera es para los profesionales independientes y también para los emprendedores que quieren compartir su conocimiento con los demás cualquiera que este sea.  Para las personas que buscan soluciones inmediatas, prácticas, tanto para ellas, su familia u hogar como su negocio o empresa.</center></hr></font></p>
                                    <hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                            </div><!--fin de  .accordion-section-->
                        </div><!--fin de  .accordion-->
                        <div class="accordion">
                            <div class="accordion-section">
                                <a class="accordion-section-title active" id="accordionD" href="#accordion-D">¿Cómo ser parte de la comunidad?</a>
                                <div style="margin-left:56px; " id="accordion-D" class="accordion-section-content">
                                <p class="respuesta"><font color="#8A4B08"><hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ><center style="width: 62%;margin-left: 199px;text-align: justify;font-size: 18px;">Ingresa a link unirse como cliente. El proceso de registro es sencillo y gratuito permitiéndote ser parte de la comunidad inmediatamente.</center></hr></font></p>
                                <hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ></hr>
                                    <a  id="subindice2" class="accordion-section-title" href="#accordion-D1">¿Cuánto cuesta ser parte de la comunidad?</a>
                                    <div id="accordion-D1" class="accordion-section-content">
                                        <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Unirte a Kubera es GRATIS.  Puedes ser cliente, consultor o ambas sin costo alguno.  Solo pagas al contratar los servicios de un consultor.</center></hr></font></p>
                                        <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                    </div><!--fin de  .accordion-section-content-->
                                    <a id="subindice2" class="accordion-section-title" href="#accordion-D2">¿Cuáles son los requisitos para estar en Kubera?</a>
                                    <div id="accordion-D2" class="accordion-section-content">
                                        <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Solo tienes que ser mayor de edad para contratar o prestar tus servicios y tienes que tener acceso a internet.</center></hr></font></p>
                                        <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                    </div><!--fin de  .accordion-section-content-->
                                    <a id="subindice2" class="accordion-section-title" href="#accordion-D3">Hay una sección para Clientes y otra para Consultores. ¿Cuál es la diferencia?</a>
                                    <div id="accordion-D3" class="accordion-section-content">
                                        <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Todos somos parte de la comunidad, y todos podemos ser Consultores y Clientes.Valoramos toda forma de conocimiento, desde profesionales especializados hasta sabios del día a día. Puedes prestar tus servicios orientados a personas, familia y hogares o a negocios y empresas. Revisa aquí nuestras categorías   (Link categorías). Si tienes una habilidad o súperpoder, como lo llamamos en Kubera, puedes compartirlo con el resto registrándote como consultor de forma gratuita. Podrás crear tus anuncios y promover tu talento al mundo. Valoramos toda forma de conocimiento. (capturas de pantalla y links).  Si lo que buscas es una solución a temas cotidianos o especializados, regístrate gratis como cliente y encuentra lo que necesitas de entre los anuncios que los especialistas han creado para tí.    (capturas de pantalla y links)</center></hr></font></p>
                                        
                                    </div><!--fin de  .accordion-section-content-->
                                </div><!--fin de  .accordion-section-content-->
                            </div><!--fin de  .accordion-section-->
                        </div><!--fin de  .accordion-->  
                   
                    <br>
                </div>

                <hr id="hr3" style="width: 68%;margin-left: 177px;">

                <button  onclick="mostrar(this); return false" value="Mostrar" id="comenzandoCliente" class="btn btn-lg" onClick="comenzandoCliente();"><i class="fa fa-chevron-left"></i>COMPARTIENDO CONOCIMINETO</button>
                <a id="unirse" class="amarillo" href="#"><i class="fa unirse-icon-5"></i></a>

                <script type="text/javascript">
                function mostrar(enla) {
                  obj = document.getElementById('oculto');
                  obj.style.display = (obj.style.display == 'block') ? 'none' : 'block';
                  enla.innerHTML = (enla.innerHTML == '< COMPARTIENDO CONOCIMINETO') ? '< COMPARTIENDO CONOCIMINETO' : '< COMPARTIENDO CONOCIMINETO';
                }
                </script>

               <div id='oculto' style="display:block";> 
                <center><iframe id="video"  style="margin-top: 10px; width: 61%; height: 350px;" width="550" height="300" src="http://www.youtube.com/embed/"></iframe></center>
                <a id="numero1C" class="amarillo" href="#"><i class="fa numero1C"></i></a>
                <a id="numero2C" class="amarillo" href="#"><i class="fa numero2C"></i></a>
                <a id="numero3C" class="amarillo" href="#"><i class="fa numero3C"></i></a>
                <p class="respuesta-cliente4" style="width: 16%; margin-left: 232px; text-align: center; margin-top:16px; font-size: 15px;"><font color="#8A4B08">Los consultores han creado anuncios y perfiles para que te resulte fácil encontrarlos</font></p>
                <p class="respuesta-cliente5" style="width: 16%; margin-left: 472px; text-align: center; margin-top:-95px; font-size: 15px;"><font color="#8A4B08">Mira las reseñas, analiza el precio y condiciones del servicio. Contáctalo inmediatamente  cuando esté disponible</font></p>
                <p class="respuesta-cliente6" style="width: 19%; margin-left: 681px; text-align: center; margin-top:-117px; font-size: 15px;"><font color="#8A4B08">Haz una consulta precisa a tu experto y explícale bien tus expectativas, no olvides hacer una reseña al finalizar la sesión para ayudarlo a él y otros buscadores como tu</font></p>
                <br>
                <div style="margin-top:30px" class="text-center">

                  <div id='oculto' style="visibility:visible";>  
                    <div class="accordion">
                        <div class="accordion-section">
                            <a class="accordion-section-title active"  id="accordion1" href="#accordion-1">¿Qué es Kubera?</a>
                            <div style="margin-left:56px" id="accordion-1" class="accordion-section-content">
                            <p class="respuesta"><font id="p1" color="#8A4B08"><hr style="width: 61%;margin-left: 174px;" size="2" color="#F2F2F2" ><center style="width: 62%;margin-left: 171px;text-align: justify;font-size: 18px;">Kubera es la comunidad del intercambio de conocimiento y el desarrollo personal. El punto de encuentro de los que buscan soluciones y los que escuchan, conocen y dan soluciones. De los que crecen y quieren ver a los demás crecer. Para ello, proveemos una plataforma para que los miembros de la comunidad, pongan su conocimiento al servicio de los demás, ofreciendo soluciones en vivo y en directo a través de una video conferencia en línea.</hr></font></p>
                            </center><hr  style="width: 61%;margin-right: 303px;" size="2" color="#F2F2F2" ></hr>
                            <br>
                                <a id="subindice" class="accordion-section-title" href="#accordion-11">¿Cómo lo hacemos?</a>
                                <div id="accordion-11" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Ayudamos a los consultores a crear su perfil, a identificar sus talentos a crear sus cursos y promocionarlos. Usamos su experiencia para mejorar la plataforma. Conectamos a personas que buscan con los que ofrecen sus servicios dándole a ambos una excelente experiencia de uso.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2"></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice" class="accordion-section-title" href="#accordion-12">¿Qué hacemos?</a>
                                <div id="accordion-12" class="accordion-section-content">
                                    <p  class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Proveemos una plataforma para que los miembros de la comunidad, pongan su conocimiento al servicio de los demás, ofreciendo soluciones en vivo y en directo. Les damos un canal de venta. Proveemos una excelente experiencia de usuario para buscadores y oferentes de servicios</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                            </div><!--fin de  .accordion-section-content-->
                        </div><!--fin de  .accordion-section-->
                    </div><!--fin de  .accordion-->
                    <div class="accordion">
                        <div class="accordion-section">
                            <a   class="accordion-section-title active" id="accordion2" href="#accordion-2">¿Por qué lo hacemos?</a>
                            <div style="margin-left:56px" id="accordion-2" class="accordion-section-content">
                                <p class="respuesta"><font color="#8A4B08"><hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ><center style="width: 62%;margin-left: 199px;text-align: justify;font-size: 18px;">Queremos que la gente crezca, descubra su talento y le vaya bien. Lideramos una nueva era del trabajo y los negocios impulsada por elintercambio de conocimiento, la generosidad y la curiosidad.</center></hr></font></p>
                                <hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ></hr>
                            </div><!--fin de  .accordion-section-content-->
                        </div><!--fin de  .accordion-section-->
                    </div><!--fin de  .accordion-->
                    <div class="accordion">
                        <div class="accordion-section">
                            <a class="accordion-section-title active" id="accordion3" href="#accordion-3">¿Para quién lo hacemos?</a>
                            <div style="margin-left:56px" id="accordion-3" class="accordion-section-content">
                                <p class="respuesta"><font color="#8A4B08"><hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ><center style="width: 62%;margin-left: 199px;text-align: justify;font-size: 18px;">Para toda persona que comparten nuestros valores. Que creen como nosotros que al compartir conocmiento, todos crecemos. Kubera es para los profesionales independientes y también para los emprendedores que quieren compartir su conocimiento con los demás cualquiera que este sea.  Para las personas que buscan soluciones inmediatas, prácticas, tanto para ellas, su familia u hogar como su negocio o empresa.</center></hr></font></p>
                                <hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ></hr>
                            </div><!--fin de  .accordion-section-content-->
                        </div><!--fin de  .accordion-section-->
                    </div><!--fin de  .accordion-->
                    <div class="accordion">
                        <div class="accordion-section">
                            <a class="accordion-section-title active" id="accordion4" href="#accordion-4">¿Cómo ser parte de la comunidad?</a>
                            <div style="margin-left:56px" id="accordion-4" class="accordion-section-content">
                            <p class="respuesta"><font color="#8A4B08"><hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ><center style="width: 62%;margin-left: 199px;text-align: justify;font-size: 18px;">Ingresa a link unirse como cliente. El proceso de registro es sencillo y gratuito permitiéndote ser parte de la comunidad inmediatamente.</center></hr></font></p>
                            <hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ></hr>
                                <a  id="subindice2" class="accordion-section-title" href="#accordion-41">¿Cuánto cuesta ser parte de la comunidad?</a>
                                <div id="accordion-41" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Unirte a Kubera es GRATIS.  Puedes ser cliente, consultor o ambas sin costo alguno.  Solo pagas al contratar los servicios de un consultor.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-42">¿Cuáles son los requisitos para estar en Kubera?</a>
                                <div id="accordion-42" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Solo tienes que ser mayor de edad para contratar o prestar tus servicios y tienes que tener acceso a internet.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-43">Hay una sección para Clientes y otra para Consultores. ¿Cuál es la diferencia?</a>
                                <div id="accordion-43" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Todos somos parte de la comunidad, y todos podemos ser Consultores y Clientes.Valoramos toda forma de conocimiento, desde profesionales especializados hasta sabios del día a día. Puedes prestar tus servicios orientados a personas, familia y hogares o a negocios y empresas. Revisa aquí nuestras categorías   (Link categorías). Si tienes una habilidad o súperpoder, como lo llamamos en Kubera, puedes compartirlo con el resto registrándote como consultor de forma gratuita. Podrás crear tus anuncios y promover tu talento al mundo. Valoramos toda forma de conocimiento. (capturas de pantalla y links).  Si lo que buscas es una solución a temas cotidianos o especializados, regístrate gratis como cliente y encuentra lo que necesitas de entre los anuncios que los especialistas han creado para tí.    (capturas de pantalla y links)</center></hr></font></p>
                                    
                                </div><!--fin de  .accordion-section-content-->
                            </div><!--fin de  .accordion-section-content-->
                        </div><!--fin de  .accordion-section-->
                    </div><!--fin de  .accordion-->
                    </div>
                    <br>
                    <hr id="hr2" style="width: 68%;margin-left: 177px;">
                    <button id="volver1" class="btn btn-lg" onClick="volver();"><i class="fa fa-chevron-left"></i> Volver</button>
                    <button id="volver2" class="btn btn-lg" onClick="ayuda();">Ayuda como consultor <i class="fa fa-chevron-right"></i></button>
                </div><br>
                <div class="col-md-12">
                    <div class="col-md-6 text-center tutoriales1">
                        <a class="cafe" href="">Videos y Tutoriales</a>
                    </div>
                    <div class="col-md-6 text-center blog1">
                        <a class="cafe" href="">Blog</a>
                        <br>
                    </div>
                </div><br>
                 <div style="margin-left:30px" class="col-md-6">
                    <a href="#"><font color="#A4A4A4" size=3><DIV ALIGN=center>
                    <br>
                    <em><u id="codigo" style="margin-left: 22px;"><a id="link2" href="http://kubera.co/codigo-etica/">Código de conducta</a></u></em></DIV></font></a>
                </div>
                 <div class="col-md-4">
                    <a href="#"><font color="#A4A4A4" size=3><DIV ALIGN=center>
                     <br>       
                    <em><u id="terminos" style="margin-left: -14px;"> <a id="link2" href="http://kubera.co/terminos-condiciones/">Términos, Condiciones y Políticas de Privacidad</a></u></em></DIV></font></a>
                </div>
                <!--<div class="col-md-4">
                    AQUI CHAT
                </div>-->
           
        </div><br>
        <!-- Termina el Contenedor de la pagina -->
        <!-- Comienza el Footer -->
        <footer>
        <div class="wrapper">
            <div class="footer col-md-12">
                <div class="col-md-6 ayuda">
                    <img src="assets/img/logo_kubera_amarillo.png"><br><br>
                    <p class="amarillo">¡Queremos que te vaya bien!</p><br><br>
                    <a class="btn boton-unete btn-lg" href="#"> Únete gratis ahora<i class="fa fa-chevron-right"></i></a><br><br>
                    <p class="blanco">¿Necesitas ayuda?</p>
                    <p class="amarillo">soporte@kubera.co</p>
                    <!--<p class="amarillo">1 800 KUBERA</p>-->
                </div>
                <div class="col-md-3">
                <p id="textoPoder" class="blanco">Todos tenemos un SÚPER PODER, algo en lo que somos realmente buenos. Aquí puedes ganar dinero compartiendo ese conocimiento y encontrar SOLUCIONES directas a lo que estás buscando.</p>
                    <!--<a href=""></a>
                    <div id="">
                        <p class="amarillo">LA EMPRESA</p>
                        <p class="blanco">Acerca de</p>
                        <p class="blanco">Contacto</p>
                        <p class="blanco">Empleo</p>
                        <p class="blanco">Ayuda</p>
                        <p class="blanco">Blog</p>
                    </div>-->
                </div>
                <div class="col-md-3 servicio">
                     <a href=""></a>
                     <div id="">
                        <p class="amarillo">EL SERVICIO</p>
                        <p class="blanco">Categorías</p>
                        <!--<p class="blanco">Preguntas frecuentes</p>-->
                        <!--<p class="blanco">Planes y tarifas</p>-->
                        <p class="blanco"><a id="link" href="http://kubera.co/terminos-condiciones/">Terminos y condiciones</a></p>
                        <p class="blanco"><a id="link" href="http://kubera.co/codigo-etica/">Código de ética y conducta</a></p>
                    </div>
                </div>
                <div class="col-md-12 text-center redes">
                    <p class="amarillo">Siguenos en: </p>
                    <a href="#"><img src="assets/img/fb.png"></a>
                    <a href="#"><img src="assets/img/tw.png"></a>
                    <a href="#"><img src="assets/img/in.png"></a></br></br>
                    <p class="blanco">© Kubera, Inc.</p>
                </div>
            </div>
        </div>
        </footer>
        <!-- Termina el Footer -->
        <!-- Boton back to top -->
        <a href="#" class="back-to-top" style="display: block;"><i class="fa fa-angle-up"></i></a>
    </body>
    <!-- Termina el Body -->
    <script type='text/javascript' data-cfasync='false'>window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: 'ea6cfbb6-15d7-4d7b-be89-90cb5988fa97', f: true }); done = true; } }; })();</script>
</html>
