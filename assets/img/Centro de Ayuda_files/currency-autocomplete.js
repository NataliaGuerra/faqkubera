$(function(){
  var currencies = [
    { value: '¿Qué es Kubera?', data: 'KUBERA' },
    { value: '¿Por qué lo hacemos?', data: 'HACEMOS' },
    { value: '¿Para quien es Kubera?', data: 'QUIEN' },
    { value: '¿Cómo ser parte de la Comunidad?', data: 'COMUNIDAD' },
    { value: '¿Cuánto cuesta ser parte de la comunidad?', data: 'COMUNIDAD' },
    { value: '¿Cuáles son los requisitos para estar en Kubera?', data: 'REQUISITOS' },
    { value: 'Hay una sección para Clientes y otra para Consultores. ¿Cuál es la diferencia?', data: 'DIFERENCIA' },
  ];
  
  // setup autocomplete function pulling from currencies[] array
  $('#mainSearcherBox').autocomplete({
    lookup: currencies,
    onSelect: function (suggestion) {
      var thehtml = '<strong>Currency Name:</strong> ' + suggestion.value + ' <br> <strong>Symbol:</strong> ' + suggestion.data;
      $('#outputbox').html(thehtml);
    }
  });
  

});