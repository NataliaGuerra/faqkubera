 
$(document).ready(function ($) {

	$("#accordion-1").css("display","block");
	

	
	/** Crear nuevo scroll **/
	$("html").niceScroll({
		scrollspeed: 100,
		mousescrollstep: 38,
		cursorwidth: 5,
		cursorborder: 0,
		cursorcolor: '#333',
		autohidemode: true,
		zindex: 999999999,
		horizrailenabled: false,
		cursorborderradius: 0
	});

	/** Scroll animacion **/
    var offset = 60;
    var duration = 500;
    $(window).scroll(function() {
        if ($(this).scrollTop() > offset) {
            $('.back-to-top').fadeIn(400);
        } else {
            $('.back-to-top').fadeOut(400);
        }
    });

    /** Click to back to top **/
    $('.back-to-top').click(function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, 600);
        return false;
    });

    /** Click on logo **/
    $(".logo").click(function(){
    	 window.location="http://www.kubera.co";
    });

    $("#clientesImg").click(function(){
    	 window.location="cliente.php#comenzandoCliente2";
    });

    $("#consultor").click(function(){
    	 window.location="consultores.php#comenzandoCliente2";
    });
    $("#consultor1").click(function(){
    	 window.location="consultores.php#comenzandoCliente2";
    });
     $("#consultor2").click(function(){
    	 window.location="consultores.php#comenzandoCliente2";
    });

    /** Acordiones, dependiendo de cuantos hay en la pregunta crear uno y cambiar el numero **/
    /** Acordion 1 **/
    	$('.accordion-section-title').click(function(e) {
	        // Grab current anchor value
	        var currentAttrValue = $(this).attr('href');
	 
	        if($(e.target).is('.active')) {
	            $(this).removeClass('active');
        		$(this).next().slideUp(300).removeClass('open');
	        }else {
	        	$(this).removeClass('active');
	        	console.log("no activo");
        		$(this).next().slideUp(300).removeClass('open');
        		console.log("no open");
	            // Add active class to section title
	            $(this).addClass('active');
	            console.log("activo");
	            // Open up the hidden content panel
	            
	            $(currentAttrValue).slideDown(300).addClass('open'); 
	            console.log(currentAttrValue);
	        }
	 
	        e.preventDefault();
	    });
	/** Termina Acordiones **/ 
	// Hover de Header.
	$(".header .wrapper #unirse").hover(function() {
		$(".unirse-icon").css("background","url(./assets/img/unirse-over.png) no-repeat center right");
		$("#unirse").css("color","white");
	});
	$(".header .wrapper #ingresar").hover(function() {
		$(".ingresar-icon").css("background","url(./assets/img/ingresar-over.png) no-repeat center right");
		$("#ingresar").css("color","white");
	});
	$(".header #buscar").hover(function() {
		$(".buscar-icon").css("background","url(./assets/img/eye-over.png) no-repeat center right");
		$("#buscar").css("color","white");
	});
	// Hover out de header.
	$(".header .wrapper #unirse").mouseleave(function() {
		$(".unirse-icon").css("background","url(./assets/img/unirse.png) no-repeat center right");
		$("#unirse").css("color","#ffc63d");
	});
	$(".header .wrapper #ingresar").mouseleave(function() {
		$(".ingresar-icon").css("background","url(./assets/img/ingresar.png) no-repeat center right");
		$("#ingresar").css("color","#ffc63d");
	});
	$(".header #buscar").mouseleave(function() {
		$(".buscar-icon").css("background","url(./assets/img/eye.png) no-repeat center right");
		$("#buscar").css("color","#ffc63d");
	});

	

	//Hover de Clientes y consultores.
	$("#clientesImg").hover(function() {
		$("#clientesImg").attr("src","assets/img/ico_clientes_2.png");
	});

	$("#clientesImg").mouseleave(function() {
		$("#clientesImg").attr("src","assets/img/ico_clientes.png");
	});
	$("#consultor").hover(function() {
		$("#consultor").attr("src","assets/img/ico_consultores_2.png");
	});

	$("#consultor").mouseleave(function() {
		$("#consultor").attr("src","assets/img/ico_consultores.png");
	});
	
	/** comienza Funcion para cambiar el header **/
    (function() {

		var docElem = document.documentElement,
			didScroll = false,
			changeHeaderOn = 100;
			document.querySelector('header');

		function init() {
			window.addEventListener( 'scroll', function() {
				if( !didScroll ) {
					didScroll = true;
					setTimeout( scrollPage, 250 );
				}
			}, false );
		}

		function scrollPage() {
			var sy = scrollY();
			if ( sy >= changeHeaderOn ) {
				$('.top-bar').slideUp(300);
				$("header").addClass("fixed-header");
				$("header").removeClass("header");
				$("#buscar").hide();
				$(".logo").css("background-image","url(./assets/img/logo_kubera_color.png)");
				$("#unirse").css("color","#6b3f23");
				$(".unirse-icon").css("background","url(./assets/img/unirse2.png) no-repeat center right");
				$("#ingresar").css("color","#6b3f23");
				$(".ingresar-icon").css("background","url(./assets/img/ingresar2.png) no-repeat center right");
				$(".fixed-header #buscar").hover(function() {
					$(".buscar-icon").css("background","url(./assets/img/ico_ojo_cafe.png) no-repeat center right");
					$("#buscar").css("color","#6b3f23");
				});
				$(".fixed-header #unirse").hover(function() {
					$(".unirse-icon").css("background","url(./assets/img/unirse2.png) no-repeat center right");
					$("#unirse").css("color","#6b3f23");
				});
				$(".fixed-header #ingresar").hover(function() {
					$(".ingresar-icon").css("background","url(./assets/img/ingresar2.png) no-repeat center right");
					$("#ingresar").css("color","#6b3f23");
				});
			}else {
				$('.top-bar').slideDown(300);
				$("header").removeClass("fixed-header");
				$("header").addClass("header");
				$("#buscar").show();
				$(".logo").css("background-image","url(./assets/img/logo_kubera_amarillo.png)");
				$("#buscar").css("color","#ffc63d");
				$(".buscar-icon").css("background","url(./assets/img/eye.png) no-repeat center right");
				$("#unirse").css("color","#ffc63d");
				$(".unirse-icon").css("background","url(./assets/img/unirse.png) no-repeat center right");
				$("#ingresar").css("color","#ffc63d");
				$(".ingresar-icon").css("background","url(./assets/img/ingresar.png) no-repeat center right");
				$(".header #buscar").hover(function() {
					$(".buscar-icon").css("background","url(./assets/img/eye.png) no-repeat center right");
					$("#buscar").css("color","#ffc63d");
				});
				$(".header #unirse").hover(function() {
					$(".unirse-icon").css("background","url(./assets/img/unirse.png) no-repeat center right");
					$("#unirse").css("color","#ffc63d");
				});
				$(".header #ingresar").hover(function() {
					$(".ingresar-icon").css("background","url(./assets/img/ingresar.png) no-repeat center right");
					$("#ingresar").css("color","#ffc63d");
				});
			}
			didScroll = false;
		}
		function scrollY() {
			return window.pageYOffset || docElem.scrollTop;
		}
		init();
	})();
	/** Termina funcion para cambiar el Header **/

});

/** Search Form **/
function search(form_name) {
    var $form = $('#'+form_name);
    var value = $('#'+form_name+' input').val();
    if(validarCampoNoVacio(value)){
        $form.attr('action','buscar.php');
        $form.submit();
        return true;
    }
    else {
       	alert("Vacio");
        return false;
    }
};

/** Realiza la validacion de los campos de un formulario antes de enviarlo **/
function justValidateForm(form_name, id_messages) {
    var form_data=$('#'+form_name).serializeArray();
    var error_free=true;
    for (var input in form_data){
        if(form_data[input]['name'].indexOf('current_step') == -1) {
            var element = $('#id_' + form_data[input]['name']);
            var valid = element.hasClass("valid");
            //console.log('-> Validando ' + String(form_data[input]['name']) + ' has valid: '+String(valid));
            if (!valid && element && element.attr('id')) {
                element.addClass("error");
                error_free = false;
            }
        }
    }

    if (!error_free) {
        //console.log('El formulario es INVALIDO');
        try {
            if(id_messages) {
                $('#'+id_messages).html('Hay errores en el formulario');
            }
            else {
                $('#form-errors').html('Hay errores en el formulario');
            }
        }catch(e) {console.log(e);}
        return false;
    }
    else{
        //console.log('Es valido el formulario ----------------');
        $('#form-errors').html('');
        return true;
    }
};

/** Volver a la pagina anterior histori -1 **/
function volver(){
	window.history.back();
}

/** Valida campo no vacio **/
function validarCampoNoVacio(string) {
    return string && string.trim().length > 0;
};

