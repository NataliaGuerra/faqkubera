<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title>Centro de Ayuda</title>
        <link rel="shortcut icon" href="assets/img/favicon.ico" />
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:800,700,600,400' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="assets/css/estilos.css">
        <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
        <link rel="stylesheet" type="text/css" href="assets/css/acordion.css">
        <script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/js/jquery-ui.js"></script>
        <script type="text/javascript" src="assets/js/jquery.nicescroll.min.js"></script>
        <script type="text/javascript" src="assets/js/jquery.autocomplete.min.js"></script>
        <script type="text/javascript" src="assets/js/currency-autocomplete.js"></script>
        <script type="text/javascript" src="assets/js/funciones.js"></script>
    </head>
    <body>
        <!-- Comienza el Body -->
        <!-- Comienza Header -->
        <header class="navbar-header header">
            <div class="wrapper">
                <div class="logo" style="margin-top: 26px;"></div>
                <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-navbar" aria-controls="bs-navbar" aria-expanded="false"> 
                <span class="sr-only">Toggle navigation</span> 
                <span class="icon-bar"></span> 
                <span class="icon-bar"></span> 
                <span class="icon-bar"></span> 
                </button>
                <nav class="navbar-collapse collapse">
                    <a id="buscar" class="amarillo" href="#">Buscar<i class="fa buscar-icon"></i></a>
                    <a id="unirse" class="amarillo" href="#">Unirse<i class="fa unirse-icon"></i></a>
                    <a id="ingresar" class="amarillo" href="#">Ingresar<i class="fa ingresar-icon"></i></a>
                </nav>
            </div>
            <nav style="height: 1px;" aria-expanded="false" id="bs-navbar" class=" collapse"> 
            <ul class="nav navbar-nav" style="text-align: center;"> 
            <li class="active"><a href="#">Buscar<i class="fa buscar-icon"></i></a></li> 
            <li><a href="#">Unirse<i class="fa unirse-icon"></i></a></li>
            <li><a href="#">Ingresar<i class="fa ingresar-icon"></i></a></li> 
            </ul> 
            </nav>
        </header>
        <!-- Termina Header -->
        <!-- Comienza el contendor de la pagina -->
        <div class="container">     
            <div class="col-md-12">
            <br>
            <FONT COLOR="#6b3f23" id="ruta"><b><i class="fa fa-chevron-left"></i> Inicio / El Servicio / Cómo Funciona</b></FONT><br>
                <b><h2 style="font-size: 45px;font-weight: 700;margin-top: 20px;">Bienvenido al centro de ayuda</h2></b> 
                <FONT style="font-size: 30px;font-weight: normal;margin-left: 358px;" FACE="arial" SIZE=5 COLOR="#6b3f23" id="subtitulo">¡Nos gusta que te vaya bien!</FONT>
                <center><iframe style="margin-top: 10px;" width="750" height="400" src="http://www.youtube.com/embed/"></iframe></center>
                <div class="container buscar" style="width: 80%;">
                <form id="form" name="form" class="buscar-consultor" action="buscar.php" method="POST" style="position: relative;">
                    <div id="selection" class="blanco"  style="font-size: 16px; font-weight:600">¿En qué podemos ayudarte?</div><br>
                        <input id="mainSearcherBox" value="" name="mainSearcherBox" class="busqueda" type="text" placeholder="Escribe aquí el tema sobre el que necesitas más información" onkeydown="">
                        <button style="font-size: 14px;" class="empezar-busqueda" type="submit">Buscar</button>
                    <div id="unibox-suggest-box" style="min-width: 810px; max-width: 398px;"></div><div id="unibox-invisible"></div></form>
                </div>
                <div class="bottom-arrow"></div>
                <div class="col-md-12 text-center">
                   <center><div style="min-width: 310px; max-width: 198px;  margin-left:250px; margin-top:30px" id="clientes" class="col-md-7">
                        <img id="clientesImg" style="margin-left: -172px; width:90%;" src="assets/img/ico_clientes.png">
                        <h3 style="margin-left: -168px;" id="h5Clientes" class="cafe">Clientes</h3>
                    </div>
                    </center>
                    <center> 
                    <div style="margin-top:30px; margin-left: 55px;" id="consultores" class="col-md-1">
                        <img style="width: 423%; margin-top: 1px; margin-left: -18px;" id="consultor2" src="assets/img/ico_consultores.png">
                        <h3 style="margin-left: 58px;" id="h5Consultores" class="cafe">Consultores</h3>
                    </div>
                    </center>
                </div>
                 <button  onclick="mostrar(this); return false" value="Mostrar" id="comenzando1" class="btn btn-lg" onClick="comenzando();"><i class="fa fa-chevron-left"></i> COMENZANDO</button>
                 <a id="unirse" class="amarillo" href="#"><i class="fa unirse-icon-7"></i></a>
                
                


                <div style="margin-top:9px" class="text-center">

                <div id='oculto' style="display:block";>   
                    <div class="accordion">
                        <div class="accordion-section">
                            <a class="accordion-section-title active"  id="accordion1" href="#accordion-1">¿Qué es Kubera?</a>
                            <div style="margin-left:56px" id="accordion-1" class="accordion-section-content">
                            <p class="respuesta"><font id="p1" color="#8A4B08"><hr style="width: 61%;margin-left: 174px;" size="2" color="#F2F2F2" ><center style="width: 62%;margin-left: 171px;text-align: justify;font-size: 18px;">Kubera es la comunidad del intercambio de conocimiento y el desarrollo personal. El punto de encuentro de los que buscan soluciones y los que escuchan, conocen y dan soluciones. De los que crecen y quieren ver a los demás crecer. Para ello, proveemos una plataforma para que los miembros de la comunidad, pongan su conocimiento al servicio de los demás,  ganando dinero al ofrecer soluciones en vivo y en directo a través de una video conferencia en línea.</hr></font></p>
                            </center><hr  style="width: 61%;margin-right: 303px;" size="2" color="#F2F2F2" ></hr>
                            <br>
                                <a id="subindice" class="accordion-section-title" href="#accordion-11">¿Qué hacemos?</a>
                                <div id="accordion-11" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Proveemos una plataforma para que los miembros de la comunidad, pongan su conocimiento al servicio de los demás, ofreciendo soluciones en vivo y en directo. Brindamos un canal de ventas, de contacto, un espacio en el cual puedes darte a conocer al mundo entero. Facilitamos el proceso de cobros y pagos. Ayudamos a promocionar los servicios así como a que encuentres soluciones. Queremos que te vaya bien y por tanto estamos preocupados tanto de que los consultores crezcan, presten servicios de calidad e impacto y que como cliente encuentres soluciones directas a lo que buscas, siempre con la mejor experiencia. </center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2"></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice" class="accordion-section-title" href="#accordion-12">¿Cómo lo hacemos?</a>
                                <div id="accordion-12" class="accordion-section-content">
                                    <p  class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Ayudamos a los consultores a crear su perfil, a identificar sus talentos a crear sus cursos o consultorías y promocionarlos. Usamos la experiencia de la comunidad y sus necesidades para mejorar la plataforma. Conectamos a personas que buscan con los que ofrecen sus servicios dándole a ambos una excelente experiencia de uso.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a   class="accordion-section-title" id="subindice" href="#accordion-2">¿Por qué lo hacemos?</a>
                                <div style="" id="accordion-2" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ><center style="width: 62%;margin-left: 199px;text-align: justify;font-size: 18px;">Queremos que la gente crezca, descubra su talento y le vaya bien. Lideramos una nueva era del trabajo y los negocios impulsada por elintercambio de conocimiento, la generosidad y la curiosidad.</center></hr></font></p>
                                    <hr style="width: 61%;margin-left: 199px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                            </div><!--fin de  .accordion-section-content-->
                        </div><!--fin de  .accordion-section-->
                    </div><!--fin de  .accordion-->
                            
                    <div class="accordion">
                        <div class="accordion-section">
                            <a class="accordion-section-title active" id="accordion4" href="#accordion-4">¿Cómo ser parte de la comunidad?</a>
                            <div style="margin-left:56px; display:block;" id="accordion-4" class="accordion-section-content">
                                <a  id="subindice2" class="accordion-section-title" href="#accordion-41">¿Cómo me uno a Kubera?</a>
                                <div id="accordion-41" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Ingresa a link "Unirse como cliente" o "Unirse como consultor". El proceso de registro es sencillo y gratuito permitiéndote ser parte de la comunidad inmediatamente.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a  id="subindice2" class="accordion-section-title" href="#accordion-44">¿Cuánto cuesta ser parte de la comunidad?</a>
                                <div id="accordion-44" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Unirte a Kubera es GRATIS.  Puedes ser cliente, consultor o ambas sin costo alguno. Solo pagas al contratar los servicios de un consultor.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-42">¿Cuáles son los requisitos para estar en Kubera?</a>
                                <div id="accordion-42" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Tienes que ser mayor de edad para contratar o prestar tus servicios. Tienes que tener un dispositivo a través del que puedas conectarte a internet.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-43">Hay una sección para Clientes y otra para Consultores. ¿Cuál es la diferencia?</a>
                                <div id="accordion-43" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Todos somos parte de la comunidad, y todos podemos ser Consultores y Clientes. Valoramos toda forma de conocimiento, desde profesionales especializados hasta sabios del día a día. Puedes prestar tus servicios orientados a personas, familias y hogares así como a negocios y empresas. Revisa aquí <a href="http://kubera.co/#main-slide">nuestras categorías</a>. Si tienes una habilidad o súperpoder, como lo llamamos en Kubera, puedes compartirlo con el resto registrándote como consultor de forma gratuita. Podrás crear tus anuncios y promover tu talento al mundo. Si lo que buscas es una solución a temas cotidianos o especializados, regístrate gratis como cliente y encuentra lo que necesitas de entre los anuncios que los especialistas han creado para tí.</center></hr></font></p>     
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-45">Conozco gente que podría prestar servicios como conultor, ¿Cómo les refiero?</a>
                                <div id="accordion-45" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Gracias! Kubera crece con el aporte de la comunidad así que valoramos mucho que nos refieras consultores y clientes.  Por favor escríbenos un correo a <a href="mailto:daniela@kubera.com">daniela@kubera.co</a> o nuestro chat de servicio</center></hr></font></p>    
                                </div><!--fin de  .accordion-section-content-->
                            </div><!--fin de  .accordion-section-content-->
                        </div><!--fin de  .accordion-section-->
                    </div><!--fin de  .accordion-->

                    <div class="accordion">
                        <div class="accordion-section">
                            <a class="accordion-section-title active" id="accordion51" href="#accordion-51">Cuánto Cuesta</a>
                            <div style="margin-left:56px; display:block;" id="accordion-51" class="accordion-section-content">
                                <a  id="subindice2" class="accordion-section-title" href="#accordion-46">¿Cuáles son los medios de pago que puedo utilizar en Kubera?</a>
                                <div id="accordion-46" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Aceptamos todas las tarjetas de crédito de las marcas internacionales más conocidas.  Dependiendo tu país de residencia, también aceptamos transferencias bancarias. En Colombia por ejemplo a través de la red PSE.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a  id="subindice2" class="accordion-section-title" href="#accordion-47">¿Qué costos tiene el servicio?</a>
                                <div id="accordion-47" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">El precio lo decide el consultor y lo acepta el cliente al momento de realizar la compra del servicio.  Depende del perfil y experiencia, la dificultad del tema y tiempo de la sesión fundamentalmente.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                            </div><!--fin de  .accordion-section-content-->
                        </div><!--fin de  .accordion-section-->
                    </div><!--fin de  .accordion-->

                    <div class="accordion">
                        <div class="accordion-section">
                            <a class="accordion-section-title active" id="accordion52" href="#accordion-52">Cómo se Usa el Sistema</a>
                            <div style="margin-left:56px; display:block;" id="accordion-52" class="accordion-section-content">
                                <a  id="subindice2" class="accordion-section-title" href="#accordion-48">¿Qué necesito para utilizar el sistema de Kubera?</a>
                                <div id="accordion-48" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Para comenzar, tan solo necesitas una computadora y una conexión a Internet de minimo 512Kbps. Vas a poder ver y navegar por la página desde tu móvil o tablet, sin embargo para realizar una video conferencia será necesario un computador o laptop. Pronto podrás descargar nuestra aplicación móvil Android e IOS.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a  id="subindice2" class="accordion-section-title" href="#accordion-49">¿Qué navegadores puedo utilizar?</a>
                                <div id="accordion-49" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Puedes acceder desde las últimas versiones de Internet Explorer, Firefox, Safari o Chrome.  Preferimos Firefox.  Necesitarás habilitar los pop-ups en tu navegador y tener cookies habilitados.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                                <a id="subindice2" class="accordion-section-title" href="#accordion-50">¿Qué necesito para realizar la video conferencia?</a>
                                <div id="accordion-50" class="accordion-section-content">
                                    <p class="respuesta"><font color="#8A4B08"><hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ><center style="width: 59%; text-align: justify;font-size: 18px; margin-left:212px;">Debes entrar desde tu laptop o computador. Para realizar la sesión ONLIVE, requieres Adobe Flash Player. Si no lo tiene instalado, instálelo en el enlace siguiente: <a href="http://get.adobe.com/es/flashplayer/">http://get.adobe.com/es/flashplayer/</a>. Requieres una conexión a internet de al menso 512 Kbps.  tu computador debe tener una cámara, parlantes y micrófonos o mejor aún si usas audífonos con micrófono durante la sesión.</center></hr></font></p>
                                    <hr style="width: 57%;margin-left: 220px;" size="2" color="#F2F2F2" ></hr>
                                </div><!--fin de  .accordion-section-content-->
                            </div><!--fin de  .accordion-section-content-->
                        </div><!--fin de  .accordion-section-->
                    </div><!--fin de  .accordion-->
            
                    <br>
                    <hr id="hr2" style="width: 68%;margin-left: 177px;">
                    <button id="volver" class="btn btn-lg" onClick="volver();"><i class="fa fa-chevron-left"></i> Volver</button>
                    <button id="volver" class="btn btn-lg" onClick="ayuda();">Ayuda como consultor <i class="fa fa-chevron-right"></i></button>
                </div><br>
                <div class="col-md-12">
                    <div class="col-md-6 text-center tutoriales">
                        <a class="cafe cafe1" href="">Videos y Tutoriales</a>
                    </div>
                    <div class="col-md-6 text-center blog">
                        <a class="cafe" href="">Blog</a>
                        <br>
                    </div>
                <br>
                <div style="margin-left:30px" class="col-md-6">
                    <a href="#"><font color="#A4A4A4" size=3><DIV ALIGN=center>
                    <br>
                    <em><u id="codigo" style="margin-left: 52px;"><a id="link2" href="http://kubera.co/codigo-etica/">Código de conducta</a></u></em></DIV></font></a>
                </div>
                 <div class="col-md-4">
                    <a href="#"><font color="#A4A4A4" size=3><DIV ALIGN=center>
                     <br>       
                    <em><u id="terminos" style="margin-left: 22px;"> <a id="link2" href="http://kubera.co/terminos-condiciones/">Términos, Condiciones y Políticas de Privacidad</a></u></em></DIV></font></a>
                </div>
                <!--<div class="col-md-4">
                    AQUI CHAT
                </div>-->
            </div></div>
        </div><br>
        <!-- Termina el Contenedor de la pagina -->
        <!-- Comienza el Footer -->
        <footer>
        <div class="wrapper">
            <div class="footer col-md-12">
                <div class="col-md-6 ayuda">
                    <img src="assets/img/logo_kubera_amarillo.png"><br><br>
                    <p class="amarillo">¡Queremos que te vaya bien!</p><br><br>
                    <a class="btn boton-unete btn-lg" href="#"> Únete gratis ahora<i class="fa fa-chevron-right"></i></a><br><br>
                    <p class="blanco">¿Necesitas ayuda?</p>
                    <p class="amarillo">soporte@kubera.co</p>
                    <!--<p class="amarillo">1 800 KUBERA</p>-->
                </div>
                <div class="col-md-3">
                <p id="textoPoder" class="blanco">Todos tenemos un SÚPER PODER, algo en lo que somos realmente buenos. Aquí puedes ganar dinero compartiendo ese conocimiento y encontrar SOLUCIONES directas a lo que estás buscando.</p>
                    <!--<a href=""></a>
                    <div id="">
                        <p class="amarillo">LA EMPRESA</p>
                        <p class="blanco">Acerca de</p>
                        <p class="blanco">Contacto</p>
                        <p class="blanco">Empleo</p>
                        <p class="blanco">Ayuda</p>
                        <p class="blanco">Blog</p>
                    </div>-->
                </div>
                <div class="col-md-3 servicio">
                     <a href=""></a>
                     <div id="">
                        <p class="amarillo">EL SERVICIO</p>
                        <p class="blanco">Categorías</p>
                        <!--<p class="blanco">Preguntas frecuentes</p>-->
                        <!--<p class="blanco">Planes y tarifas</p>-->
                        <p class="blanco"><a id="link" href="http://kubera.co/terminos-condiciones/">Terminos y condiciones</a></p>
                        <p class="blanco"><a id="link" href="http://kubera.co/codigo-etica/">Código de ética y conducta</a></p>
                    </div>
                </div>
                <div class="col-md-12 text-center redes">
                    <p class="amarillo">Siguenos en: </p>
                    <a href="#"><img src="assets/img/fb.png"></a>
                    <a href="#"><img src="assets/img/tw.png"></a>
                    <a href="#"><img src="assets/img/in.png"></a></br></br>
                    <p class="blanco">© Kubera, Inc.</p>
                </div>
            </div>
        </div>
        </footer>
        <!-- Termina el Footer -->
        <!-- Boton back to top -->
        <a href="#" class="back-to-top" style="display: block;"><i class="fa fa-angle-up"></i></a>
    </body>
    <script type="text/javascript">
    function mostrar(enla) {
      obj = document.getElementById('oculto');
      obj.style.display = (obj.style.display == 'block') ? 'none' : 'block';
      enla.innerHTML = (enla.innerHTML == 'COMENZANDO') ? 'COMENZANDO' : 'COMENZANDO';
    }
    </script>
    <script type='text/javascript' data-cfasync='false'>window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: 'ea6cfbb6-15d7-4d7b-be89-90cb5988fa97', f: true }); done = true; } }; })();</script>
    <!-- Termina el Body -->
</html>
